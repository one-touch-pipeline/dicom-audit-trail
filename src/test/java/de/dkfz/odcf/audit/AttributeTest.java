/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit;

import de.dkfz.odcf.audit.xml.*;
import java.util.*;
import org.junit.*;

import static org.junit.Assert.*;

public class AttributeTest {

    static Attribute<String> standard, empty, quotedNonreq, emptyRequired, quotedName;
    static Attribute<Integer> intAttr;
    static Attribute<List<Integer>> listAttr;

    @BeforeClass
    public static void setUpClass() {
        standard = new Attribute("name", "value");
        empty = new Attribute("name");
        quotedNonreq = new Attribute("name", "\"value", false);
        emptyRequired = new Attribute("name", null, true);
        intAttr = new Attribute("name", 1);
        listAttr = new Attribute("name", Arrays.asList(1, 2, 3));
        quotedName = new Attribute("\"name\"=", "value");
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void dShouldBeInvalid() {
        assertTrue(standard.isValid());
        assertTrue(empty.isValid());
        assertTrue(quotedNonreq.isValid());
        assertFalse(emptyRequired.isValid());
        assertTrue(intAttr.isValid());
        assertTrue(listAttr.isValid());
    }

    @Test
    public void toXMLShouldWork() {
        assertEquals(standard.toXmlString(), "name=\"value\"");
        assertEquals(empty.toXmlString(), "name=\"\"");
        assertEquals(quotedNonreq.toXmlString(), "name=\"\\\"value\"");
        assertEquals(intAttr.toXmlString(), "name=\"1\"");
        assertEquals(listAttr.toXmlString(), "name=\"" + listAttr.getValue().toString() + "\"");
    }

    @Test(expected = IllegalOperationException.class)
    public void dShouldThrowExceptionOnParse() {
        emptyRequired.toXmlString();
    }

    @Test
    public void nameShouldBeClean() {
        assertEquals(quotedName.toXmlString(), "name=\"value\"");
    }

    @Test
    public void equalsShouldBeTrue() {
        assertEquals(standard, empty);
        assertEquals(empty, intAttr);
        assertEquals(intAttr, listAttr);
    }
}
