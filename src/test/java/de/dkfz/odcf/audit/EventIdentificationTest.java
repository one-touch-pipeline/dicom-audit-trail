/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit;

import de.dkfz.odcf.audit.xml.layer.*;
import de.dkfz.odcf.audit.xml.layer.EventIdentification.*;
import java.time.*;
import org.junit.*;

import static org.junit.Assert.*;

/**
 *
 * @author Florian Tichawa
 */
public class EventIdentificationTest {

    private static EventIdentification event;

    public EventIdentificationTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        AuditFactory.loadLibrary(AuditFactory.class.getResourceAsStream("config/library.xml"));
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        event = new EventIdentification();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void newInstanceShouldBeInvalidWithoutChildrenAndAttributes() {
        assertFalse(event.isValid());
        event.withEventId("12345", "Login");
        assertFalse(event.isValid());
        event.withActionCode(EventActionCode.EXECUTE);
        assertFalse(event.isValid());
        event.withEventOutcomeIndicator(EventOutcomeIndicator.SUCCESS);
        assertFalse(event.isValid());
        event.withDateTime(Instant.now());
        assertFalse(event.isValid());
        event.withEventTypeCode("1234", "Auth");
        assertTrue(event.isValid());
    }
}
