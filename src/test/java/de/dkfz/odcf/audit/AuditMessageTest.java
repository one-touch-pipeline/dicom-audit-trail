/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit;

import de.dkfz.odcf.audit.xml.layer.*;
import de.dkfz.odcf.audit.xml.layer.AuditSourceIdentification.*;
import de.dkfz.odcf.audit.xml.layer.EventIdentification.*;
import de.dkfz.odcf.audit.xml.layer.ParticipantObjectIdentification.*;
import java.net.*;
import java.time.*;
import org.junit.*;

import static org.junit.Assert.*;

public class AuditMessageTest {

    private static AuditMessage message;

    @Before
    public void setUp() {
        message = new AuditMessage();
    }

    @Test
    public void minimalObjectShouldBeValid() {
        //Empty message should not be valid
        assertFalse(message.isValid());
        //Message with only EventId should not be valid
        assertFalse(message.withEventId(new EventIdentification()
                .withEventId("12345", "Login")
                .withActionCode(EventActionCode.EXECUTE)
                .withDateTime(Instant.now())
                .withEventOutcomeIndicator(EventOutcomeIndicator.SUCCESS)
                .withEventTypeCode("1234", "Auth")).isValid());
        //Message with only EventId and Participant should not be valid
        assertFalse(message.withParticipant(new ActiveParticipant().withUserId("12346").asRequestor()).isValid());
        //Message with all three elements (ID, Participant, Source) should be valid
        assertTrue(message.withAuditSource(new AuditSourceIdentification()
                .withAuditSourceId("Google")
                .withTypeCode(AuditSourceTypeCode.ENDUSER_DISPLAY)).isValid());
    }

    @Test
    public void bigObjectShouldBeValid() throws UnknownHostException {
        //Empty message should not be valid
        assertFalse(message.isValid());
        //Message with only EventId should not be valid
        assertFalse(message.withEventId(new EventIdentification()
                .withEventId("12345", "Login")
                .withActionCode(EventActionCode.EXECUTE)
                .withDateTime(Instant.now())
                .withEventOutcomeIndicator(EventOutcomeIndicator.SUCCESS)
                .withEventTypeCode("110111", "Login successful")).isValid());
        //Message with only EventId and Participant should not be valid
        assertFalse(message.withParticipant(new ActiveParticipant()
                .withUserId("12346")
                .withNetworkAccessPoint(Inet4Address.getByName("127.1"))
                .withRoleIdCode("ABC123", "Login successful")
                .withRoleIdCode("ABC124", "Login with admin privileges")
                .withMediaIdentifier("5532", "3.5' floppy")
                .asRequestor()).isValid());
        //Message with only EventId and two Participants should not be valid
        assertFalse(message.withParticipant(new ActiveParticipant().withUserId("12347").asRequestor()).isValid());
        //Message with only EventId, two Participants and Object should not be valid
        assertFalse(message.withObject(new ParticipantObjectIdentification()
                .withId("DCF733")
                .withIdTypeCode(ObjectIdTypeCode.STUDY)
                .withTypeCodeRole(ObjectTypeCode.PERSON, ObjectTypeCodeRole.PATIENT)
                .withName("Studie DCF 733")
                .withLifeCycle(ObjectLifeCycle.DEIDENTIFICATION)
                .withDescription("Patient 0")
                .withDetail("Gender", "Male")
                .withDetail("Age", "23")
                .withMPPS("12345")
                .withSopInstance("123")
                .withSopInstance("345")
                .withStudy("AC123456")
                .asAnonymized(true)
                .asEncrypted(true)).isValid());
        //Message with only EventId, two Participants and two Objects should not be valid
        assertFalse(message.withObject(new ParticipantObjectIdentification()
                .withId("DCF734")
                .withIdTypeCode(ObjectIdTypeCode.SOP)
                .withTypeCodeRole(ObjectTypeCode.ORGANIZATION, ObjectTypeCodeRole.PROVIDER)
                .withName("Studie DCF 734")).isValid());
        //Message with only EventId, two Participants, two Objects and Source should be valid
        assertTrue(message.withAuditSource(new AuditSourceIdentification()
                .withAuditSourceId("Google")
                .withTypeCode(AuditSourceTypeCode.ENDUSER_DISPLAY)).isValid());
        //Call internal validation method to ensure validity
        message.toString();
    }
}
