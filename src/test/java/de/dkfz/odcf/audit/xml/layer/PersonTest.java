/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.xml.layer;

import org.junit.*;
import static org.junit.Assert.*;

public class PersonTest {

    private static final Person P1 = new Person("a", "b");
    private static final Person P1_2 = Person.parse(P1.toString());
    private static final Person P2 = new Person(null, "a", null, "b", null);
    private static final Person P2_2 = new Person("", "a", "", "b", "");
    private static final Person P3 = new Person("a", "a", null, "b", null);
    private static final Person P4 = new Person("a", "b", "c", "d", "e");
    private static final Person P4_2 = Person.parse(P4.toString());

    @Test
    public void testEquals() {
        assertEquals(P1, P2);
        assertEquals(P1, P2_2);
        assertEquals(P2, P2_2);
        assertNotEquals(P1, P3);
    }

    @Test
    public void testToString() {
        assertEquals("b^a^^^", P1.toString());
        assertEquals("d^b^c^a^e", P4.toString());
    }

    @Test
    public void testParse() {
        assertNotSame(P1, P1_2);
        assertEquals(P1, P1_2);

        assertNotSame(P4, P4_2);
        assertEquals(P4, P4_2);
    }

}
