/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.xml.layer;

import de.dkfz.odcf.audit.*;
import de.dkfz.odcf.audit.AuditFactory.Success;
import de.dkfz.odcf.audit.xml.*;
import java.time.*;
import org.junit.*;
import org.mockito.*;

import static de.dkfz.odcf.audit.AuditFactory.*;
import static org.junit.Assert.*;

public class AuditFactoryTest {

    private static AuditMessage msg;
    private static AuditMessage childMsg;
    private static AuditMessage loginMsg;
    private static AuditMessage userCreatedMsg;
    private static AuditMessage permissionRevokedMsg;

    @BeforeClass
    public static void setUpClass() {
        AuditFactory.loadLibrary(AuditFactory.class.getResourceAsStream("config/library.xml"));

        msg = Mockito.spy(AuditFactory.ofType("Test"));
        Mockito.when(msg.isFormatAndContentValid()).thenReturn(true);
        msg.isValid();
        childMsg = Mockito.spy(AuditFactory.ofType("Test2"));
        Mockito.when(childMsg.isFormatAndContentValid()).thenReturn(true);
        childMsg.isValid();
        loginMsg = AuditFactory.ofType("User Login");
        loginMsg.isValid();
        userCreatedMsg = AuditFactory.ofType("User Added");
        permissionRevokedMsg = AuditFactory.ofType("Permission Revoked");
    }

    @Test
    public void callingExistingElementsShouldReturnNewInstances() {
        DynamicXmlObject astc1 = AuditFactory.createElement("AuditSourceTypeCode");
        DynamicXmlObject astc2 = AuditFactory.createElement("AuditSourceTypeCode");
        assertEquals(astc1, astc2);
        assertNotSame(astc1, astc2);
        assertFalse(astc1.isValid());
        assertTrue(astc2.withContent("1").isValid());
    }

    @Test
    public void callingElementsWithChildrenShouldAddReqChild() {
        DynamicXmlObject mi = AuditFactory.createElement("MediaIdentifier");
        DynamicXmlObject mt = AuditFactory.createElement("MediaType");
        assertFalse(mi.isValid());
        assertFalse(mt.isValid());
        assertTrue(mt.withAttr("csd-code", "0").withAttr("codeSystemName", "DCM").withAttr("displayName", "Nulldevice").withAttr("originalText", "/dev/null").isValid());
        assertTrue(mi.withChild(mt).isValid());
    }

    @Test(expected = IllegalOperationException.class)
    public void callingNonExistentElementsShouldThrowException() {
        AuditFactory.createElement("ThisDoesNotExist");
    }

    @Test
    public void callingTemplateShouldReturnTemplatedInstance() {
        assertTrue(msg.getAttrs().contains(new Attribute("csd-code")));
    }

    @Test
    public void callingTemplateShouldAddRequiredChildren() {
        assertTrue(msg.getChildren().size() > 0);
    }

    @Test
    public void constraintsShouldBeCalledInOrder() {
        assertEquals("banana", msg.getChildren().get(0).getChildren().get(0).getAttr("csd-code")
                .map(Attribute::getValue).orElse("Not banana"));
    }

    @Test
    public void childTemplatesShouldCopyParentConstraints() {
        assertEquals("apple", childMsg.getChildren().get(0).getChildren().get(0).getAttr("csd-code")
                .map(Attribute::getValue).orElse("Not apple"));
    }

    @Test
    public void printLoginMsg() {
        loginMsg.getEventId().withEventOutcomeIndicator(EventIdentification.EventOutcomeIndicator.SUCCESS).withDateTime(Instant.now());
        loginMsg.withAuditSource(new AuditSourceIdentification()
                .withTypeCode(AuditSourceIdentification.AuditSourceTypeCode.OTHER)
                .withAuditSourceId("123455688"))
                .withParticipant(new ActiveParticipant()
                        .withUserId("123456789"));
        loginMsg.toString();
    }

    @Test
    public void userSecurityEventsShouldOnlyRequireActiveParticipantsAndOutcomeAndDate() {
        assertFalse(userCreatedMsg.isValid());
        userCreatedMsg.getEventId().withEventOutcomeIndicator(EventIdentification.EventOutcomeIndicator.SUCCESS).withDateTime(Instant.now());
        assertFalse(userCreatedMsg.withAuditSource(new AuditSourceIdentification()
                .withTypeCode(AuditSourceIdentification.AuditSourceTypeCode.OTHER)
                .withAuditSourceId("123441111")).isValid());
        assertTrue(userCreatedMsg
                .withParticipant(new ActiveParticipant().withUserId("1338"))
                .withParticipant(new ActiveParticipant().withUserId("1337").asRequestor()).isValid());

        assertFalse(permissionRevokedMsg.isValid());
        assertTrue(permissionRevokedMsg.getEventId().withEventOutcomeIndicator(EventIdentification.EventOutcomeIndicator.SUCCESS).withDateTime(Instant.now()).isValid());
        assertFalse(permissionRevokedMsg.withAuditSource(new AuditSourceIdentification()
                .withTypeCode(AuditSourceIdentification.AuditSourceTypeCode.OTHER)
                .withAuditSourceId("123441111")).isValid());
        assertFalse(permissionRevokedMsg
                .withParticipant(new ActiveParticipant().withUserId("1338"))
                .withParticipant(new ActiveParticipant().withUserId("1337").asRequestor()).isValid());
        permissionRevokedMsg.withAttr(Selector.parse("ParticipantObjectIdentification"), "ParticipantObjectID", "Login");
        Selector.parse("ParticipantObjectName").stream(permissionRevokedMsg).forEach(obj -> obj.setContent("Log into OTP"));
        permissionRevokedMsg.toString();
    }

    @Test
    public void includingNewTemplateFileShouldNotOverwriteOldTemplates() {
        assertEquals(Success.PARTIAL, loadTemplates(AuditFactory.class.getResourceAsStream("config/templates.xml")));
    }
}
