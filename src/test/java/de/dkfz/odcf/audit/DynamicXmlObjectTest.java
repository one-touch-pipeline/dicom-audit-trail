/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit;

import de.dkfz.odcf.audit.xml.Attribute;
import de.dkfz.odcf.audit.xml.DynamicXmlObject;
import de.dkfz.odcf.audit.xml.IllegalOperationException;
import de.dkfz.odcf.audit.xml.Selector;
import org.junit.*;

import static org.junit.Assert.*;
import static de.dkfz.odcf.audit.xml.XmlEntity.*;

public class DynamicXmlObjectTest {

    static DynamicXmlObject emptyRoot;
    static DynamicXmlObject rootWithAttrs;
    static DynamicXmlObject rootWithChild;
    static DynamicXmlObject emptyChild;
    static DynamicXmlObject childWithAttrs;
    static DynamicXmlObject illegalRoot;
    static DynamicXmlObject illegalSuperRoot;
    static DynamicXmlObject loneChild;

    static DynamicXmlObject dynamicRoot;
    static DynamicXmlObject testObj;
    static DynamicXmlObject prioObj;

    static Attribute testAttr;
    static Attribute cestAttr;
    static Attribute lowPrioObj;

    static Selector childSelector;
    static Selector childInRootSelector;

    @BeforeClass
    public static void setUpClass() {
        childSelector = Selector.parse("Child");
        childInRootSelector = Selector.parse("Root Child");
    }

    @Before
    public void setUp() {
        emptyRoot = new DynamicXmlObject("Root");
        rootWithAttrs = new DynamicXmlObject("Root")
                .withAttr("name", "value");
        emptyChild = new DynamicXmlObject("Child");
        childWithAttrs = new DynamicXmlObject("Child")
                .withAttr("name", 1);
        rootWithChild = new DynamicXmlObject("Root")
                .withChild(emptyChild)
                .withChild(childWithAttrs);
        illegalRoot = new DynamicXmlObject("Root")
                .withAttr(new Attribute("illegal", null, true));
        illegalSuperRoot = new DynamicXmlObject("Superroot")
                .withChild(illegalRoot);
        loneChild = new DynamicXmlObject("Child");

        dynamicRoot = new DynamicXmlObject("DynRoot");

        testAttr = new Attribute("Test");
        cestAttr = new Attribute("cest");
        testObj = new DynamicXmlObject("Test");
        prioObj = new DynamicXmlObject("Eest", -1);
        lowPrioObj = new Attribute("Ast", 1);
    }

    @Test
    public void illegalRootShouldBeInvalid() {
        assertTrue(emptyRoot.isValid());
        assertTrue(rootWithAttrs.isValid());
        assertTrue(rootWithAttrs.isValid());
        assertFalse(illegalRoot.isValid());
        assertFalse(illegalSuperRoot.isValid());
    }

    @Test
    public void toXMLShouldWork() {
        assertEquals("<Root />", emptyRoot.toXmlString());
        assertEquals("<Root name=\"value\" />", rootWithAttrs.toXmlString());
    }

    @Test
    public void streamShouldFindAllElements() {
        assertEquals(1, emptyRoot.stream().count());
        assertEquals(3, rootWithChild.stream().count());
        assertEquals(2, illegalSuperRoot.stream().count());
    }

    @Test
    public void selectorsShouldWork() {
        assertEquals(0, childSelector.stream(emptyRoot).count());
        assertEquals(1, childSelector.stream(loneChild).count());
        assertEquals(2, childSelector.stream(rootWithChild).count());
        assertEquals(0, childInRootSelector.stream(loneChild).count());
        assertEquals(2, childInRootSelector.stream(rootWithChild).count());
        assertEquals(0, childInRootSelector.stream(illegalSuperRoot).count());

        assertEquals(0, Selector.NULL.stream(rootWithChild).count());
        assertEquals(1, Selector.THIS.stream(rootWithChild).count());
    }

    @Test
    public void addingChildShouldRemoveItFromParent() {
        dynamicRoot.withChild(emptyChild);
        assertEquals(2, dynamicRoot.stream().count());
        assertTrue(emptyChild.getParent() == dynamicRoot);
        assertEquals(2, rootWithChild.stream().count());
    }

    @Test
    public void addingAtributeWithSelectorShouldFindAllMatches() {
        Attribute attr = new Attribute("newName", "newValue");
        rootWithChild.withAttr(childSelector, attr);
        assertEquals(2, rootWithChild.stream().filter(obj -> obj.getAttrs().contains(attr)).count());
    }

    @Test
    public void addingExistingAttributesShouldOverwrite() {
        emptyChild.withAttr("newName", "newValue");
        assertEquals(1, emptyChild.getAttrs().size());
        assertEquals("newValue", emptyChild.getAttrs().stream().map(a -> a.getValue()).findAny().get());
        emptyChild.withAttr("newName", "newerValue");
        assertEquals(1, emptyChild.getAttrs().size());
        assertEquals("newerValue", emptyChild.getAttrs().stream().map(a -> a.getValue()).findAny().get());
    }

    @Test
    public void usingAnySelectorShouldMatchAllObjects() {
        emptyChild.withChild(loneChild);
        assertEquals(rootWithChild.stream().count(), Selector.parse("ANY").stream(rootWithChild).count());
        assertEquals(3, Selector.parse("ANY Child").stream(rootWithChild).count());
        assertEquals(3, Selector.parse("Root ANY").stream(rootWithChild).count());
    }

    @Test
    public void usingThisOrEmptySelectorShouldCreateNullSelector() {
        assertEquals(Selector.NULL, Selector.parse("THIS"));
        assertEquals(Selector.NULL, Selector.parse(""));
    }

    @Test(expected = IllegalOperationException.class)
    public void parsingIllegalElementShouldThrowException() {
        illegalRoot.toXmlString();
    }

    @Test
    public void parsingIllegalParentShouldHandDown() {
        try {
            illegalSuperRoot.toXmlString();
            fail("No exception was thrown");
        } catch (IllegalOperationException ex) {
            assertEquals(new Attribute("illegal", null, true), ex.getSource());
        }
    }

    @Test
    public void naturalOrderingWithoutPriorityShouldSortByTagName() {
        assertEquals(sgn(emptyChild.getName().compareTo(emptyRoot.getName())), sgn(emptyChild.compareTo(emptyRoot)));
        assertEquals(sgn(testAttr.getName().compareTo(emptyChild.getName())), sgn(testAttr.compareTo(emptyChild)));
        assertEquals(sgn(testAttr.getName().compareTo(cestAttr.getName())), sgn(testAttr.compareTo(cestAttr)));
        assertEquals(sgn(testAttr.getName().compareTo(testObj.getName())), sgn(testAttr.compareTo(testObj)));
    }

    @Test
    public void naturalOrderingWithPriorityShouldIgnoreTagName() {
        assertTrue(prioObj.compareTo(testObj) < 0);
        assertTrue(prioObj.compareTo(cestAttr) < 0);
        assertTrue(prioObj.compareTo(lowPrioObj) < 0);
    }
}
