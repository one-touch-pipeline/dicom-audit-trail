/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit;

import de.dkfz.odcf.audit.xml.*;
import de.dkfz.odcf.audit.xml.Constraint.Type;
import de.dkfz.odcf.audit.xml.layer.*;
import java.io.*;
import java.util.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public class AuditFactory {

    /**
     * An Enum representing the result of the library loading process.
     */
    public static enum Success {
        /**
         * All templates have been loaded successfully.
         */
        FULL,
        /**
         * Some templates have been loaded successfully, while other have been
         * skipped (E.g. to prevent duplicates).
         */
        PARTIAL,
        /**
         * Total failure, usually due to an IO or format error.
         */
        NONE
    };

    private static final Map<String, DynamicXmlObject> LIBRARY = new HashMap<>();
    private static final Map<String, List<Constraint>> TEMPLATES = new HashMap<>();
    private static final Map<String, String> TEMPLATE_PARENTS = new HashMap<>();

    private AuditFactory() {
    }

    /**
     * Loads the template library from the given InputStream.<br>
     * This method can not override any already loaded template definitions to
     * prevent code injection at runtime.
     *
     * @param stream InputStream pointing at the library file
     * @return The success state, refer to the documentation of
     * AuditFactory.SUCCESS for details
     */
    public static Success loadTemplates(InputStream stream) {
        try {
            Document libSource = loadDocument(stream);

            boolean fullSuccess = true;
            NodeList templates = libSource.getElementsByTagName("Template");
            for (int i = 0; i < templates.getLength(); i++) {
                Node template = templates.item(i);
                String name = template.getAttributes().getNamedItem("name").getNodeValue();
                if (!TEMPLATES.containsKey(name)) {
                    TEMPLATES.put(name, new LinkedList<>());
                    if (template.getAttributes().getNamedItem("parent") != null) {
                        TEMPLATE_PARENTS.put(name, template.getAttributes().getNamedItem("parent").getNodeValue());
                    }

                    for (int j = 0; j < template.getChildNodes().getLength(); j++) {
                        Node constraint = template.getChildNodes().item(j);
                        if (Arrays.asList("Attribute", "Child", "Content").contains(constraint.getNodeName())) {
                            Node nameAttr = constraint.getAttributes().getNamedItem("name");
                            Node valueAttr = constraint.getAttributes().getNamedItem("value");
                            Node selectorAttr = constraint.getAttributes().getNamedItem("selector");
                            switch (constraint.getNodeName()) {
                                case "Attribute":
                                    TEMPLATES.get(name).add(new Constraint(selectorAttr.getNodeValue(), nameAttr.getNodeValue(),
                                            constraint.getAttributes().getNamedItem("value").getNodeValue()));
                                    break;
                                case "Child":
                                    TEMPLATES.get(name).add(new Constraint(selectorAttr.getNodeValue(), nameAttr.getNodeValue(), Type.CHILD));
                                    break;
                                case "Content":
                                    TEMPLATES.get(name).add(new Constraint(selectorAttr.getNodeValue(), valueAttr.getNodeValue(), Type.CONTENT));
                                    break;
                            }
                        }
                    }
                } else {
                    fullSuccess &= false;
                }
            }
            return fullSuccess ? Success.FULL : Success.PARTIAL;
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            return Success.NONE;
        }
    }

    /**
     * Loads the tag library from the given InputStream.<br>
     * This method can not override any already loaded tag definitions to
     * prevent code injection at runtime.
     *
     * @param stream InputStream pointing at the library file
     * @return The success state, refer to the documentation of
     * AuditFactory.SUCCESS for details
     */
    public static Success loadLibrary(InputStream stream) {
        try {
            Document libSource = loadDocument(stream);

            boolean fullSuccess = true;
            NodeList tags = libSource.getElementsByTagName("Tag");
            for (int i = 0; i < tags.getLength(); i++) {
                Node tag = tags.item(i);
                DynamicXmlObject obj = new DynamicXmlObject(tag.getAttributes().getNamedItem("name").getNodeValue(),
                        tag.getAttributes().getNamedItem("priority") == null ? 0 : Integer.parseInt(tag.getAttributes().getNamedItem("priority").getNodeValue()));
                for (int j = 0; j < tag.getChildNodes().getLength(); j++) {
                    Node subNode = tag.getChildNodes().item(j);
                    switch (subNode.getNodeName()) {
                        case "Attr":
                            obj.withReqAttr(subNode.getAttributes().getNamedItem("name").getNodeValue());
                            break;
                        case "Content":
                            obj.withReqContent(true);
                            break;
                        case "Child":
                            obj.withReqChild(subNode.getAttributes().getNamedItem("name").getNodeValue());
                            break;
                    }
                }
                if (!LIBRARY.containsKey(obj.getName())) {
                    LIBRARY.putIfAbsent(obj.getName(), obj);
                } else {
                    fullSuccess = false;
                }
            }
            return fullSuccess ? Success.FULL : Success.PARTIAL;
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            return Success.NONE;
        }
    }

    /**
     * Creates a new AuditMessage of the given type.<br>
     * This method searches the internal template for the supplied String and
     * attempts to build a new AuditMessage based on the found constraints
     *
     * @param type Template name of the new instance
     * @return A new AuditMessage from the template library
     * @throws IllegalOperationException If the requested template name could
     * not be found
     */
    public static AuditMessage<? extends AuditMessage> ofType(String type) throws IllegalOperationException {
        return ofTypeUnvalidated(type).applyConstraints();
    }

    /**
     * Creates a new TemplatedAuditMessage of the given type without using
     * TemplatedAuditMessage.validate() on it.<br>
     * This method is required for internal application, like the parent
     * attribute of templates.
     *
     * @param type Template name of the new instance
     * @return A new TemplatedAuditMessage with the non-applied Constraints from
     * the template library
     * @throws IllegalOperationException If the requested template name could
     * not be found
     */
    private static TemplatedAuditMessage ofTypeUnvalidated(String type) throws IllegalOperationException {
        if (TEMPLATES.isEmpty()) {
            loadTemplates(AuditFactory.class.getResourceAsStream("config/templates.xml"));
        }
        if (type == null) {
            return null;
        } else if (TEMPLATES.containsKey(type)) {
            TemplatedAuditMessage parent = ofTypeUnvalidated(TEMPLATE_PARENTS.get(type));
            if (parent != null) {
                return new TemplatedAuditMessage(parent).withAllConstraints(TEMPLATES.get(type));
            } else {
                return new TemplatedAuditMessage().withAllConstraints(TEMPLATES.get(type));
            }
        } else {
            throw new IllegalOperationException(null, "Unknown template \"" + type + "\".");
        }
    }

    /**
     * Internal helper to load XML documents.
     *
     * @param stream InputStream pointing at the XML resource
     * @return The parsed content from the input resource
     * @throws ParserConfigurationException If the parser has a configuration
     * error
     * @throws SAXException If the document is not well-formed
     * @throws IOException If the stream cannot be read
     */
    private static Document loadDocument(InputStream stream) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = dbFactory.newDocumentBuilder();
        return builder.parse(stream);
    }

    /**
     * Creates a new element with the given tag name.<br>
     * This method tries to find and instantiate the given type as subclass of
     * DynamicXmlObject via reflection first, and defaults to loading it from
     * the library if the first attempt fails in any way.
     *
     * @param type Tag name of the new instance
     * @return A new DynamicXmlObject with the given tag name
     * @throws IllegalOperationException If the requested tag name could not be
     * found
     */
    public static DynamicXmlObject createElement(String type) throws IllegalOperationException {
        if (LIBRARY.isEmpty()) {
            loadLibrary(AuditFactory.class.getResourceAsStream("config/library.xml"));
        }
        try {
            Class clazz = Class.forName("de.dkfz.odcf.audit.xml.layer." + type);
            return (DynamicXmlObject) clazz.newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | ClassCastException ex) {
            if (LIBRARY.containsKey(type)) {
                return LIBRARY.get(type).copy();
            } else {
                throw new IllegalOperationException(null, "Unknown XML element \"" + type + "\".");
            }
        }
    }

    /**
     * Creates a new element with the given tag name and the default csdCode
     * attributes.
     *
     * @param type Tag name of the new instance
     * @param csdCode Value of the csd-code attribute
     * @param csn Value of the codeSystemName attribute
     * @param originalText Value of the originalText attribute
     * @param displayName Value of the displayName attribute, may be null
     * @return
     * @throws IllegalOperationException
     */
    public static DynamicXmlObject createElementWithCodeSystemAttributes(String type, String csdCode, String csn, String originalText, String displayName) throws IllegalOperationException {
        return createElement(type)
                .withAttr("csd-code", csdCode)
                .withAttr("codeSystemName", csn)
                .withAttr("originalText", originalText)
                .withAttr("displayName", displayName == null ? originalText : displayName);
    }
}
