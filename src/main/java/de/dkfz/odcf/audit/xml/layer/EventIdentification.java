/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.xml.layer;

import de.dkfz.odcf.audit.*;
import de.dkfz.odcf.audit.xml.*;
import java.time.*;
import java.time.format.*;

/**
 * A layer class representing a DICOM EventIdentification.<br>
 * Required attributes:<br>
 * - EventActionCode<br>
 * - EventDateTime<br>
 * - EventOutcomeIndicator<br>
 * Required children:<br>
 * - EventID<br>
 *
 * @author Florian Tichawa
 */
public class EventIdentification extends DynamicXmlObject<EventIdentification> {

    public enum EventActionCode {
        CREATE('C'), READ('R'), UPDATE('U'), DELETE('D'), EXECUTE('E');

        private final char charVal;

        EventActionCode(char c) {
            charVal = c;
        }

        public char charVal() {
            return charVal;
        }

        @Override
        public String toString() {
            return "" + charVal();
        }
    };

    /**
     * Enum describing the outcom of a result: Success, Minor Failure, Serious
     * Failure, Critical Failure. Refer to the instance documentations for
     * definitions of the specific result.
     */
    public enum EventOutcomeIndicator {
        /**
         * No error occurred during execution.
         */
        SUCCESS(0),
        /**
         * Alias: Warning.<br>
         * Some error(s) occurred, but they were not severe enough to prevent
         * the execution.<br>
         * The process may have resulted in partially invalid or unexpected
         * results.
         */
        MINOR_FAILURE(4),
        /**
         * Alias: Error.<br>
         * The error was too severe to guarantee a safe execution.<br>
         * Usually the process was canceled or crashed during execution.
         */
        SERIOUS_FAILURE(8),
        /**
         * Alias: Crash.<br>
         * The error not only prevented the execution of the current process,
         * but may also crash some or all of the connected processes.
         */
        CRITICAL_FAILURE(12);

        private final int numVal;

        EventOutcomeIndicator(int numVal) {
            this.numVal = numVal;
        }

        public int intVal() {
            return numVal;
        }

        @Override
        public String toString() {
            return "" + intVal();
        }
    }

    /**
     * Default constructor
     */
    public EventIdentification() {
        super("EventIdentification", -1);
        withReqAttr("EventActionCode");
        withReqAttr("EventDateTime");
        withReqAttr("EventOutcomeIndicator");
        withReqChild("EventTypeCode");
        withReqChild("EventID");
    }

    /**
     * Sets this instance's EventActionCode.
     *
     * @param code EventActionCode to set
     * @return This for chain calls
     */
    public EventIdentification withActionCode(EventActionCode code) {
        return withAttr("EventActionCode", code);
    }

    /**
     * Sets this instance's timestamp.
     *
     * @param instant Timestamp to set
     * @return This for chain calls
     */
    public EventIdentification withDateTime(Instant instant) {
        return withAttr(new Attribute<>("EventDateTime", instant, DateTimeFormatter.ISO_INSTANT::format));
    }

    /**
     * Sets this instance's EventOutcomeIndicator.
     *
     * @param outcome OutcomeIndicator to set
     * @return This for chain calls
     */
    public EventIdentification withEventOutcomeIndicator(EventOutcomeIndicator outcome) {
        return withAttr("EventOutcomeIndicator", outcome);
    }

    /**
     * Shorthand method for withEventId(csdCode, "DCM", originalText).
     *
     * @param csdCode Event ID Code according to DCM
     * @param originalText Event name
     * @return This for chain calls
     */
    public EventIdentification withEventId(String csdCode, String originalText) {
        return withEventId(csdCode, "DCM", originalText);
    }

    /**
     * Shorthand method for withEventId(csdCode, csn, originalText, null).
     *
     * @param csdCode Event ID Code according to given CSN
     * @param csn CodeSystemName
     * @param originalText Event name
     * @return This for chain calls
     */
    public EventIdentification withEventId(String csdCode, String csn, String originalText) {
        return withEventId(csdCode, csn, originalText, null);
    }

    /**
     * Adds an Event ID to this instance.
     *
     * @param csdCode Event ID Code according to given CSN
     * @param csn CodeSystemName
     * @param originalText Event ID name
     * @param displayName Human readable event name
     * @return This for chain calls
     */
    public EventIdentification withEventId(String csdCode, String csn, String originalText, String displayName) {
        return withChild(AuditFactory.createElementWithCodeSystemAttributes("EventID", csdCode, csn, originalText, displayName));
    }

    /**
     * Shorthand method for withEventTypeCode(csdCode, "DCM", originalText).
     *
     * @param csdCode Event Type Code according to DCM
     * @param originalText Event Type name
     * @return This for chain calls
     */
    public EventIdentification withEventTypeCode(String csdCode, String originalText) {
        return withEventTypeCode(csdCode, "DCM", originalText);
    }

    /**
     * Shorthand method for withEventTypeCode(csdCode, csn, originalText, null).
     *
     * @param csdCode Event Type Code according to given CSN
     * @param csn CodeSystemName
     * @param originalText Event Type name
     * @return This for chain calls
     */
    public EventIdentification withEventTypeCode(String csdCode, String csn, String originalText) {
        return withEventTypeCode(csdCode, csn, originalText, null);
    }

    /**
     * Adds an Event Type to this instance.
     *
     * @param csdCode Event Type Code according to given CSN
     * @param csn CodeSystemName
     * @param originalText Event Type name
     * @param displayName Human readable event name
     * @return This for chain calls
     */
    public EventIdentification withEventTypeCode(String csdCode, String csn, String originalText, String displayName) {
        return withChild(AuditFactory.createElementWithCodeSystemAttributes("EventTypeCode", csdCode, csn, originalText, displayName));
    }
}
