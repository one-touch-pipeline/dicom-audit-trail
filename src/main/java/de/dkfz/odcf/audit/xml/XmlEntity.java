/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.xml;

import java.util.*;

/**
 * The core template for XML elements.
 * 
 * Warning: This class has a non-standard implementation of natural ordering:
 * Objects of this class will be sorted by their <code>naturalOrderingPriority</code> (Ascending) first,
 * and by their <code>name</code> (Alphabetic order) second.
 *
 * @author Florian Tichawa
 */
public abstract class XmlEntity implements Comparable<XmlEntity> {

    private final String name;
    private final Integer naturalOrderingPriority;

    protected XmlEntity(XmlEntity other) {
        this(other.name, other.naturalOrderingPriority);
    }

    protected XmlEntity(String name) {
        this(name, 0);
    }

    protected XmlEntity(String name, int naturalOrderingPriority) {
        this.name = name;
        this.naturalOrderingPriority = naturalOrderingPriority;
    }

    /**
     * Checks this instance for validity. The definition of validity depends on the element in question and might differ from case to case.
     * This method is expected to recursively check it's own children for validity.
     * 
     * @return True if and only if this element should be considered "valid"
     */
    public abstract boolean isValid();
    
    /**
     * Creates a textual XML representation of this instance that can be used to build an entire XML hierarchy representation.<br>
     * This method is expected to throw an IllegalOperationException when trying to parse an invalid (As in XmlEnitity.isValid) entity.
     * 
     * @return String representation of this element
     * @throws IllegalOperationException 
     */
    protected abstract StringBuilder toXml() throws IllegalOperationException;

    @Override
    public String toString() {
        return toXmlString().replaceAll("[\n\t]", "");
    }

    /**
     * Shorthand method for toXml().toString()
     * 
     * @return String representation of this element
     */
    public String toXmlString() {
        return toXml().toString();
    }

    /**
     * Returns this instance's tag name.
     * 
     * @return Tag name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns this instance's ordering priority. An XMLEntity is sorted by it's priority first, and by it's tag name afterwards.
     * 
     * @return Ordering priority
     */
    public Integer getNaturalOrderingPriority() {
        return naturalOrderingPriority;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.name);
        hash = 19 * hash + Objects.hashCode(this.naturalOrderingPriority);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof XmlEntity)) {
            return false;
        }
        final XmlEntity other = (XmlEntity) obj;
        return Objects.equals(this.name, other.name)
                && Objects.equals(this.naturalOrderingPriority, other.naturalOrderingPriority);
    }

    /**
     * Compares this XmlEntity to the given XmlEntity. An XmlEntity is sorted by it's priority first, and by it's tag name afterwards.
     * 
     * @param other XmlEntity to compare this instance to.
     * @return 
     */
    @Override
    public int compareTo(XmlEntity other) {
        int nameComparation = this.name.compareTo(other.name);
        return 2 * this.naturalOrderingPriority.compareTo(other.naturalOrderingPriority)
                + sgn(nameComparation);
    }

    public static int sgn(int i) {
        return (i < 0) ? -1 : ((i > 0) ? 1 : 0);
    }
}
