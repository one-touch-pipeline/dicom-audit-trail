/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.xml.layer;

import de.dkfz.odcf.audit.*;
import de.dkfz.odcf.audit.xml.*;
import java.net.*;
import java.util.*;
import java.util.stream.*;

import static de.dkfz.odcf.audit.xml.layer.ActiveParticipant.NetworkAccessPointType.*;

/**
 * A layer class representing a DICOM ActiveParticipant.<br>
 * Required attributes:<br>
 * - UserID<br>
 * - UserIsRequestor<br>
 *
 * @author Florian Tichawa
 */
public class ActiveParticipant extends DynamicXmlObject<ActiveParticipant> {

    public enum NetworkAccessPointType {
        MACHINE_NAME(1), IP_ADDRESS(2), PHONE_NUMBER(3), MAIL_ADDRESS(4), URI(5);

        private final int numVal;

        NetworkAccessPointType(int numVal) {
            this.numVal = numVal;
        }

        public int intVal() {
            return numVal;
        }

        @Override
        public String toString() {
            return String.valueOf(intVal());
        }
    }

    /**
     * Default constructor.
     */
    public ActiveParticipant() {
        super("ActiveParticipant");
        withReqAttr("UserID");
        withReqAttr("UserIsRequestor");
    }

    /**
     * Sets this instance's UserID attribute.
     *
     * @param userId user ID
     * @return This for chain calls
     */
    public ActiveParticipant withUserId(String userId) {
        return withAttr("UserID", userId);
    }

    /**
     * Sets this instance's AlternativeUserID attribute.<br>
     * The given arguments will be parsed as a semicolon separated list and
     * overwrite the current attribute value.
     *
     * @param userIds Alternative user IDs
     * @return This for chain calls
     */
    public ActiveParticipant withAlternativeUserIds(String... userIds) {
        return withAttr("AlternativeUserID", Arrays.stream(userIds).collect(Collectors.joining(";")));
    }

    /**
     * Sets this instance's UserName attribute. Please consider using
     * withUserName(Person person) instead whenever possible.
     *
     * @param uName User name
     * @return This for chain calls
     */
    public ActiveParticipant withUserName(String uName) {
        return withAttr("UserName", uName);
    }

    /**
     * Sets this instance's UserName attribute. This method is preferred over
     * withUserName(String uName).
     *
     * @param person Person object representing the user
     * @return This for chain calls
     */
    public ActiveParticipant withUserName(Person person) {
        return withUserName(person.toString());
    }

    /**
     * Sets this ActiveParticipant as requestor.<br>
     * Setting a requestor removes the requestor attribute from all current
     * siblings.<br>
     * AuditMessage.withActiveParticipant ensures that exactly one
     * ActiveParticipant can be set as requestor at any time.<br>
     * Do NOT use any other methods besides AuditMessage.withActiveParticipant
     * and ActiveParticipant.asRequestor to manage the ActiveParticipants and
     * their respective requestor attribute.
     *
     * @return This for chain calls
     */
    public ActiveParticipant asRequestor() {
        if (!isRoot()) {
            getParent().getChildren().stream()
                    .filter(child -> child.getName().equals(getName()))
                    .map(child -> (DynamicXmlObject) child)
                    .forEach(child -> child.withAttr("UserIsRequestor", false));
        }
        return withAttr("UserIsRequestor", true);
    }

    /**
     * Returns true if and only if this attribute is marked as requestor.
     *
     * @return Requestor flag
     */
    public boolean isRequestor() {
        return getAttr("UserIsRequestor").map(attr -> (Boolean) attr.getValue()).orElse(Boolean.FALSE);
    }

    /**
     * Shorthand method for withNetworkAccessPoint(address,
     * NetworkAccessPoinType.IP_ADDRESS).
     *
     * @param address IPv4 or IPv6 address
     * @return This for chain calls
     */
    public ActiveParticipant withNetworkAccessPoint(InetAddress address) {
        return withNetworkAccessPoint(address, IP_ADDRESS);
    }

    /**
     * Shorthand method for withNetworkAccessPoint(address,
     * NetworkAccessPoinType.URI).
     *
     * @param address URI address
     * @return This for chain calls
     */
    public ActiveParticipant withNetworkAccessPoint(URI address) {
        return withNetworkAccessPoint(address, URI);
    }

    /**
     * Defines this instance's NetworkAccessPoint.<br>
     * This method expects a String for PHONE_NUMBER, MAIL_ADDRESS and
     * MACHINE_NAME, a java.net.InetAddress for IP_ADDRESS and a java.net.URI
     * for URI.<br>
     * This method does NOT check the validity of the given identifier.
     *
     * @param id NAP identifier
     * @param type Specifier for the NAP identifier type
     * @return This for chain calls
     */
    public ActiveParticipant withNetworkAccessPoint(Object id, NetworkAccessPointType type) {
        boolean valid;
        switch (type) {
            case PHONE_NUMBER:
            case MAIL_ADDRESS:
            case MACHINE_NAME:
                valid = id instanceof String;
                break;
            case IP_ADDRESS:
                valid = id instanceof InetAddress;
                break;
            case URI:
                valid = id instanceof URI;
                break;
            default:
                valid = false;
                break;
        }
        if (valid) {
            return withAttr("NetworkAccessPointID", id)
                    .withAttr("NetworkAccessPointType", type);
        } else {
            return this;
        }
    }

    /**
     * Shorthand method for withRoleIdCode(csdCode, "DCM", originalText).
     *
     * @param csdCode Role ID code
     * @param originalText Role name
     * @return This for chain calls
     */
    public ActiveParticipant withRoleIdCode(String csdCode, String originalText) {
        return withRoleIdCode(csdCode, "DCM", originalText);
    }

    /**
     * Shorthand method for withRoleIdCode(csdCode, csn, originalText, null).
     *
     * @param csdCode Role ID code
     * @param csn Code System Name
     * @param originalText Role name
     * @return This for chain calls
     */
    public ActiveParticipant withRoleIdCode(String csdCode, String csn, String originalText) {
        return withRoleIdCode(csdCode, csn, originalText, null);
    }

    /**
     * Defines this instance's Role ID.<br>
     * <code>null</code> as displayName is allowed and results in displayName =
     * originalText.<br>
     * Refer to CID 402 for default IDs, use "DCM" as Code System Name for any
     * CID 402 ID.
     *
     * @param csdCode Role ID code
     * @param csn Code System Name
     * @param originalText Role name
     * @param displayName Role display name
     * @return This for chain calls
     */
    public ActiveParticipant withRoleIdCode(String csdCode, String csn, String originalText, String displayName) {
        return withChild(AuditFactory.createElement("RoleIDCode")
                .withAttr("csd-code", csdCode)
                .withAttr("codeSystemName", csn)
                .withAttr("originalText", originalText)
                .withAttr("displayName", displayName == null ? originalText : displayName));
    }

    /**
     * Shorthand method for withMediaIdentifier(csdCode, "DCM", originalText).
     *
     * @param csdCode Media ID code
     * @param originalText Media name
     * @return This for chain calls
     */
    public ActiveParticipant withMediaIdentifier(String csdCode, String originalText) {
        return withMediaIdentifier(csdCode, "DCM", originalText);
    }

    /**
     * Shorthand method for withMediaIdentifier(csdCode, csn, originalText,
     * null).
     *
     * @param csdCode Media ID code
     * @param csn Code System Name
     * @param originalText Media name
     * @return This for chain calls
     */
    public ActiveParticipant withMediaIdentifier(String csdCode, String csn, String originalText) {
        return withMediaIdentifier(csdCode, csn, originalText, null);
    }

    /**
     * Adds a Media Identifier to this instance.<br>
     * <code>null</code> as displayName is allowed and results in displayName =
     * originalText.<br>
     * Refer to CID 405 for default IDs, use "DCM" as Code System Name for any
     * CID 405 ID.
     *
     * @param csdCode Media ID code
     * @param csn Code System Name
     * @param originalText Media name
     * @param displayName Media display name
     * @return This for chain calls
     */
    public ActiveParticipant withMediaIdentifier(String csdCode, String csn, String originalText, String displayName) {
        return withChild(AuditFactory.createElement("MediaIdentifier")
                .withChild(AuditFactory.createElement("MediaType")
                        .withAttr("csd-code", csdCode)
                        .withAttr("codeSystemName", csn)
                        .withAttr("originalText", originalText)
                        .withAttr("displayName", displayName == null ? originalText : displayName)));
    }
}
