/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.xml;

import java.util.*;
import java.util.regex.*;
import java.util.stream.*;

public class XmlObject extends XmlEntity {

    private static final Pattern NEW_LINE = Pattern.compile("\n");
    private static final String EXCEPTION_PREFIX = "Trying to parse illegal XmlObject: ";

    private final Set<Attribute> attrs;
    private final Set<String> reqChildren;
    private final List<XmlObject> children;

    private String content;
    private XmlObject parent;
    private boolean reqContent;

    protected XmlObject(XmlObject other) {
        this(other.getName(),
                other.getNaturalOrderingPriority(),
                other.attrs.stream().map(Attribute::copy).collect(Collectors.toSet()),
                other.children.stream().map(XmlObject::copy).collect(Collectors.toList()),
                new HashSet<>(other.reqChildren),
                other.content,
                other.reqContent);
    }

    protected XmlObject(String name) {
        this(name, 0);
    }

    protected XmlObject(String name, int naturalOrderingPriority) {
        this(name, naturalOrderingPriority, new HashSet<>(), new LinkedList<>(), new HashSet<>(), null, false);
    }

    protected XmlObject(String name, int naturalOrderingPriority, Set<Attribute> attrs, List<XmlObject> children, Set<String> reqChildren, String content, boolean reqContent) {
        super(name, naturalOrderingPriority);
        this.children = children;
        this.attrs = new HashSet<>();
        attrs.forEach(this::addAttribute);
        this.reqChildren = reqChildren;
        this.content = content;
        this.reqContent = reqContent;
    }

    @Override
    public boolean isValid() {
        return isFormatAndContentValid()
                && attrs.stream().allMatch(Attribute::isValid)
                && children.stream().allMatch(XmlObject::isValid);
    }

    /**
     * Checks this instance for validity, ignoring the validity of it's children
     * and attributes.
     *
     * @return True if and only if this element should be considered "valid",
     * assuming that all children return true in their respective validity
     * checks.
     */
    protected boolean isFormatAndContentValid() {
        return reqChildrenAreValid()
                && reqContentIsValid()
                && childContentBlockIsValid();
    }

    /**
     * Checks whether this instance contains all children that were marked as
     * required.
     *
     * @return True if and only if every required child has a match in the
     * children list, so that reqChild.getName().equals(match.getName())
     */
    private boolean reqChildrenAreValid() {
        return reqChildren.stream().allMatch(tagName -> children.stream().anyMatch(child -> child.getName().equals(tagName)));
    }

    /**
     * Checks whether this instance possesses a text body, and whether it should
     * contain one.
     *
     * @return True if and only if the text body is either not required or
     * required and not empty
     */
    private boolean reqContentIsValid() {
        return !reqContent || (content != null && !content.isEmpty());
    }

    /**
     * Checks whether this instance has either children, or a content, or none.
     *
     * @return False if this instance contains a text body and children, True
     * otherwise.
     */
    private boolean childContentBlockIsValid() {
        return (content == null || content.isEmpty()) || children.isEmpty();
    }

    /**
     * Checks whether this instance has a parent or not.
     *
     * @return True if and only if this element does not have a parent
     */
    public boolean isRoot() {
        return parent == null;
    }

    /**
     * Returns this instance's parent, or null if none exists.
     *
     * @return This instance's parent
     */
    public XmlObject getParent() {
        return parent;
    }

    /**
     * Removes this child from it's current parent and sets this element's
     * parent to the given value.<br>
     * Calling this method with null as argument will result in detaching this
     * instance and creating a new root node.<br>
     * Do not use any other method than this to add a child to a parent to
     * ensure data integrity.
     *
     * @param parent The parent to add this to
     */
    protected void setParent(XmlObject parent) {
        if (!isRoot()) {
            getParent().children.remove(this);
        }
        if (stream().noneMatch(obj -> obj == parent)) {
            this.parent = parent;
        }
        if (!isRoot()) {
            parent.children.add(this);
        }
    }

    /**
     * Shorthand method for child.setParent(null) if
     * this.getChildren().contains(child), NOP otherwise.
     *
     * @param child Child to remove from this parent.
     */
    protected void removeChild(XmlObject child) {
        if (child.getParent() == this) {
            child.setParent(null);
        }
    }

    @Override
    protected StringBuilder toXml() {
        if (isFormatAndContentValid()) {
            StringBuilder sb = new StringBuilder("<").append(getName());
            attrs.forEach(attr -> sb.append(" ").append(attr.toXml()));
            if (children.isEmpty() && (content == null || content.isEmpty())) {
                sb.append(" />");
            } else {
                sb.append(">");
                sb.append(contentToXml());
                sb.append("</").append(getName()).append(">");
            }
            return sb;
        } else {
            throw getXmlError();
        }
    }

    /**
     * Internal helper returning the concatenated result of toXml() on every
     * child of this instance and prepending each line with tab, or returning
     * this instance's plain text content if it contains no children.
     *
     * @ return A StringBuilder containing this instance's plain text content or
     * the concatenated toXml() return value of all children
     */
    private StringBuilder contentToXml() {
        StringBuilder sb = new StringBuilder();
        if (content != null && !content.isEmpty()) {
            sb.append(content);
        } else {
            Collections.sort(children);
            children.forEach(child -> {
                sb.append("\n\t");
                StringBuilder childSb = child.toXml();
                Matcher m = NEW_LINE.matcher(childSb);
                while (m.find()) {
                    childSb.insert(m.start() + 1, "\t");
                }
                sb.append(childSb);
            });
            sb.append("\n");
        }
        return sb;
    }

    /**
     * Internal helper to identify XML errors.
     *
     * @return An IllegalOperationException describing the exact XML error
     */
    private IllegalOperationException getXmlError() {
        if (!reqChildrenAreValid()) {
            return new IllegalOperationException(this, EXCEPTION_PREFIX + "Missing required child(ren) "
                    + reqChildren.stream()
                            .filter(reqChild -> children.stream().noneMatch(child -> child.getName().equals(reqChild)))
                            .map(reqChild -> "\"" + reqChild + "\"")
                            .collect(Collectors.joining(", ")));
        } else if (!reqContentIsValid()) {
            return new IllegalOperationException(this, EXCEPTION_PREFIX + "Missing content");
        } else if (!childContentBlockIsValid()) {
            return new IllegalOperationException(this, EXCEPTION_PREFIX + "An object must not have content and children at the same time.");
        } else {
            return new IllegalOperationException(this, EXCEPTION_PREFIX + "Unknown error.");
        }
    }

    /**
     * Returns this instance's text body.
     *
     * @return This instance's text body.
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets this instance's text body to the given value.
     *
     * @param content Text to set as body
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Checks whether this instance requires a text body or not.
     *
     * @return True if and only if this instance requires a text body
     */
    protected boolean isReqContent() {
        return reqContent;
    }

    /**
     * Sets whether this instance requires a text body or not.
     *
     * @param reqContent Content required flag
     */
    protected void setReqContent(boolean reqContent) {
        this.reqContent = reqContent;
    }

    /**
     * Returns a read-only copy of this instance's children
     *
     * @return Read-only child list
     */
    public List<XmlObject> getChildren() {
        return Collections.unmodifiableList(children);
    }

    /**
     * Checks whether this instance contains an Attribute with the given name.
     *
     * @param name Attribute name
     * @return True if and only if this instance contains an Attribute with the
     * given name.
     */
    public boolean hasAttr(String name) {
        return getAttr(name).isPresent();
    }

    /**
     * Returns the first Attribute which a name matching the given name, or
     * Optional.empty() if no match was found.
     *
     * @param name Attribute name
     * @return An Optional containing the first match or Optional.empty()
     */
    public Optional<Attribute> getAttr(String name) {
        return getAttrs().stream().filter(attr -> attr.getName().equals(name) && !attr.isEmpty()).findAny();
    }

    /**
     * Returns a read-only copy of this instance's attributes.
     *
     * @return Read-only attribute set
     */
    public Set<Attribute> getAttrs() {
        return Collections.unmodifiableSet(attrs);
    }

    /**
     * Returns this instance's required children.
     *
     * @return Required children
     */
    protected final Set<String> getReqChildren() {
        return reqChildren;
    }

    /**
     * Shorthand method for addAttribute(attr, true).
     *
     * @param attr Attribute to add
     */
    protected void addAttribute(Attribute attr) {
        addAttribute(attr, true);
    }

    /**
     * Shorthand method for addAttribute(Selector.THIS, attr).
     *
     * @param attr Attribute to add
     * @param override Whether the new Attribute should override any existing
     * Attribute
     */
    protected void addAttribute(Attribute attr, boolean override) {
        addAttribute(Selector.THIS, attr, override);
    }

    /**
     * Streams this instance over the given Selector and adds a copy of the
     * given attribute to every matching XmlObject.
     *
     * @param selector Stream filter
     * @param attr Attribute to add
     * @param override Whether the new Attribute should override any existing
     * Attribute
     */
    protected void addAttribute(Selector selector, Attribute attr, boolean override) {
        selector.stream(this).filter(obj -> override || obj.attrs.stream().filter(Attribute::isValid).noneMatch(a -> a.equals(attr)))
                .forEach(obj -> {
                    Attribute a = attr.copy();
                    a.setParent(obj);
                    obj.attrs.remove(a);
                    obj.attrs.add(a);
                });
    }

    /**
     * Alias for child.setParent(this).
     *
     * @param child Child to add to this parent.
     */
    protected void addChild(XmlObject child) {
        child.setParent(this);
    }

    /**
     * Create a stream containing this XmlObject and all children (Recursively).
     *
     * @return Stream of the entire XML hierarchy, starting with this instance
     * as root
     */
    public Stream<XmlObject> stream() {
        return Stream.concat(Stream.of(this), getChildren().stream().flatMap(XmlObject::stream));
    }

    /**
     * Shorthand method for new XmlObject(this).
     *
     * @return A deep copy of this XmlObject.
     */
    public XmlObject copy() {
        return new XmlObject(this);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.attrs);
        hash = 29 * hash + Objects.hashCode(this.children);
        hash = 29 * hash + Objects.hashCode(this.reqChildren);
        hash = 29 * hash + Objects.hashCode(this.content);
        hash = 29 * hash + (this.reqContent ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final XmlObject other = (XmlObject) obj;
        if (!super.equals(obj)) {
            return false;
        }
        if (!Objects.equals(this.attrs, other.attrs)) {
            return false;
        }
        if (!Objects.equals(this.children, other.children)) {
            return false;
        }
        if (this.reqContent != other.reqContent) {
            return false;
        }
        return Objects.equals(this.reqChildren, other.reqChildren);
    }
}
