/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.xml;

import java.util.*;
import java.util.function.*;

/**
 * A basic class representing an XML Attribute.<br>
 * Warning: This class overrides the expected behavior of <code>equals()</code>.
 *
 * @author Florian Tichawa
 * @param <T> Attribute type
 */
public class Attribute<T> extends XmlEntity {

    private T value;
    private boolean required;
    private boolean templated;
    private XmlObject parent;
    private Function<T, String> parser;

    /**
     * A copy constructor, creating a deep copy of the given Attribute.
     *
     * @param other Attribute instance to copy
     */
    private Attribute(Attribute<T> other) {
        this(other.getName(), other.value, other.required, other.templated, other.parser);
    }

    /**
     * Shorthand constructor for empty attributes, forwarding the construction
     * to <code>Attribute(String name, null)</code> with value = null.
     *
     * @param name Attribute name
     */
    public Attribute(String name) {
        this(name, null);
    }

    /**
     * Shorthand constructor for filled attributes, forwarding the construction
     * to <code>Attribute(String name, T value, false)</code>.
     *
     * @param name Attribute name
     * @param value Attribute value
     */
    public Attribute(String name, T value) {
        this(name, value, false);
    }

    /**
     * Shorthand constructor for filled attributes, forwarding the construction
     * to <code>Attribute(String name, T value, boolean required, false)</code>.
     *
     * @param name Attribute name
     * @param value Attribute value
     * @param required isRequired flag
     */
    public Attribute(String name, T value, boolean required) {
        this(name, value, required, false);
    }

    /**
     * Default constructor for attributes.
     *
     * @param name Attribute name
     * @param value Attribute value
     * @param required isRequired flag
     * @param templated isTemplated flag
     */
    public Attribute(String name, T value, boolean required, boolean templated) {
        this(name, value, required, templated, t -> t == null ? "" : t.toString());
    }

    /**
     * Shorthand constructor for filled attributes with a custom parser,
     * forwarding the construction to
     * <code>Attribute(String name, T value, false, false, Function parser)</code>.
     *
     * @param name Attribute name
     * @param value Attribute value
     * @param parser Custom attribute parser
     */
    public Attribute(String name, T value, Function<T, String> parser) {
        this(name, value, false, false, parser);
    }

    /**
     * Full option constructor for attributes.
     *
     * @param name Attribute name
     * @param value Attribute value
     * @param required isRequired flag
     * @param templated isTemplated flag
     * @param parser Custom attribute parser
     */
    public Attribute(String name, T value, boolean required, boolean templated, Function<T, String> parser) {
        super(name.replace("\"", "").replace("=", ""));
        this.value = value;
        this.parser = parser;
        this.required = required;
        this.templated = templated;
    }

    /**
     * Returns true if and only if this element is empty or not required.
     *
     * @return Validity of this Attribute
     */
    @Override
    public boolean isValid() {
        return !required || value != null;
    }

    /**
     * Returns true if and only if this element was created from an template.
     *
     * @return Template flag of this Attribute
     */
    public boolean isTemplated() {
        return templated;
    }

    @Override
    protected StringBuilder toXml() {
        if (isValid()) {
            StringBuilder sb = new StringBuilder(getName()).append("=\"");
            sb.append(parser.apply(value).replace("\"", "\\\""));
            return sb.append("\"");
        } else {
            throw new IllegalOperationException(this, "Missing required Attribute in "
                    + (parent == null ? "Null Object" : parent.getClass().getSimpleName())
                    + " \"" + (parent == null ? "null" : parent.getName()) + "\"");
        }
    }

    /**
     * Returns the stored attribute value.
     *
     * @return Attribute value
     */
    public T getValue() {
        return value;
    }

    /**
     * Returns the stored attribute type.
     *
     * @return Attribute value type
     */
    public Class<?> getValueClass() {
        return value.getClass();
    }

    /**
     * Sets the stored value to <code>value</code>
     *
     * @param value New attribute value
     */
    public void setValue(T value) {
        this.value = value;
    }

    /**
     * Sets the isRequired flag to <code>required</code>.
     *
     * @param required New isRequired flag value
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

    /**
     * Sets the parser function, default is
     * <code>value -&gt; value.toString()</code>.
     *
     * @param parser New parser function
     */
    public void setParser(Function<T, String> parser) {
        this.parser = parser;
    }

    /**
     * Returns the current parent of this element if existent, or
     * <code>null</code>.
     *
     * @return Current parent
     */
    public XmlObject getParent() {
        return parent;
    }

    /**
     * Sets the current parent of this element, or changes the isRoot property
     * to <code>true</code> if <code>null</code> is given. Using or overwriting
     * this function may result in unspecified behavior, use
     * <code>XmlObject.addAttribute()</code> or
     * <code>DynamicXmlObject.withAttr()</code> instead.
     *
     * @param parent New parent or null
     */
    protected void setParent(XmlObject parent) {
        this.parent = parent;
    }

    /**
     * Shorthand method for new <code>Attribute(this)</code>.
     *
     * @return A deep copy of this Attribute.
     */
    public Attribute<T> copy() {
        return new Attribute(this);
    }

    /**
     * Checks whether this instance's value is empty.
     *
     * @return True if and only if value == null
     */
    public boolean isEmpty() {
        return value == null;
    }

    /**
     * Sets this instance's isTemplated flag.
     *
     * @param templated isTemplated flag value
     * @return This for chain calls
     */
    public Attribute<T> asTemplated(boolean templated) {
        this.templated = templated;
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(getName());
        return hash;
    }

    /**
     * Compares this object's name with the given Attribute name.<br>
     * Warning: This class overrides the expected behavior of
     * <code>equals()</code>.
     *
     * @param obj Object to compare with
     * @return Equality of this and given Attribute
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Attribute<?> other = (Attribute<?>) obj;
        return Objects.equals(this.getName(), other.getName());
    }
}
