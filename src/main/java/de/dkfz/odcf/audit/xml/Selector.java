/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.xml;

import java.util.*;
import java.util.regex.*;
import java.util.stream.*;

/**
 * A DOM Selector implementation, offering basic class selectors and an ANY
 * selector.
 *
 * @author Florian Tichawa
 */
public class Selector {

    public static final Selector NULL = new Selector("");
    public static final Selector THIS = new Selector("THIS");

    private static final Pattern ATTR_PATTERN = Pattern.compile("\\[(.+)=(.+)\\]");

    private final String[] parentSelector;
    private final String elementSelector;

    /**
     * Default constructor, creating a new Selector from the given String
     * varargs.
     *
     * @param selectors DOM Selector varargs
     */
    private Selector(String... selectors) {
        //Invert selector array
        for (int i = 0; i < selectors.length / 2; i++) {
            String temp = selectors[i];
            selectors[i] = selectors[selectors.length - i - 1];
            selectors[selectors.length - i - 1] = temp;
        }
        this.parentSelector = Arrays.copyOfRange(selectors, 1, selectors.length);
        this.elementSelector = selectors[0];
    }

    /**
     * Create a stream containing all matches within the child hierarchy of
     * <code>root</code>.
     *
     * @param root Root element
     * @return Stream containing all matches
     */
    public Stream<XmlObject> stream(XmlObject root) {
        if (this == NULL) {
            return Stream.empty();
        } else if (this == THIS) {
            return Stream.of(root);
        } else {
            return root.stream().filter(this::matches);
        }
    }

    /**
     * Tests <code>obj</code> against this selector.
     *
     * @param obj Element to test against
     * @return True if and only if <code>obj</code> matches this selector
     */
    public boolean matches(XmlObject obj) {
        if (this == NULL) {
            return false;
        } else if (this == THIS) {
            return true;
        } else if (elementSelector.equals("ANY") || elementMatches(elementSelector, obj)) {
            Stack<String> stack = new Stack<>();
            Arrays.stream(parentSelector).forEach(stack::push);
            for (XmlObject current = obj.getParent(); current != null && !stack.isEmpty(); current = current.getParent()) {
                if (stack.peek().equals("ANY") || elementMatches(stack.peek(), current)) {
                    stack.pop();
                }
            }
            return stack.isEmpty();
        } else {
            return false;
        }
    }

    /**
     * Matches the given Selector (Only single-layered Selectors, no XPath style
     * Selectors) against the given XmlObject.
     *
     * @param selector Selector to match
     * @param obj Object to test
     * @return True if the object satisfies the given Selector
     */
    private static boolean elementMatches(String selector, XmlObject obj) {
        List<Attribute<String>> stackAttrs = new LinkedList<>();
        Matcher m = ATTR_PATTERN.matcher(selector);
        while (m.find()) {
            stackAttrs.add(new Attribute<>(m.group(1), m.group(2)));
        }
        if (!stackAttrs.isEmpty()) {
            selector = selector.substring(0, selector.indexOf('['));
        }
        return obj.getName().equals(selector)
                && stackAttrs.stream()
                        .allMatch(stackAttr -> obj.getAttr(stackAttr.getName())
                        .filter(currAttr -> currAttr.getValue().toString().equals(stackAttr.getValue()))
                        .isPresent());
    }

    /**
     * Creates a selector from the given String. Nested classes have to be
     * separated by a space. <code>THIS</code> is a reserved word for
     * Selector.THIS, using this word with <code>Selector.parse()</code> will
     * result in a parsing failure. <code>ANY</code> can be used as equivalent
     * to the CSS selector <code>*</code>.
     *
     * @param selector Selector string to parse
     * @return The parsed Selector or Selector.NULL on failure
     */
    public static Selector parse(String selector) {
        if (selector != null && selector.trim().length() > 0) {
            String[] selectors = selector.trim().split(" ");
            if (selectors.length > 0 && Arrays.stream(selectors).noneMatch(s -> s.equals("THIS"))) {
                return new Selector(selectors);
            }
        }
        return NULL;
    }

    /**
     * Shorthand method for <code>Selector.parse(selector).stream(root)</code>.
     *
     * @param selector Selector string to parse
     * @param root Root object for the stream
     * @return A stream containing all matches to the selector or an empty
     * stream on failure
     */
    public static Stream<XmlObject> stream(String selector, XmlObject root) {
        return parse(selector).stream(root);
    }
}
