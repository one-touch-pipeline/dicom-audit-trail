/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.xml.layer;

import java.util.*;

public class Person {

    private final String title;
    private final String firstName;
    private final String middleName;
    private final String lastName;
    private final String suffix;

    public Person(String firstName, String lastName) {
        this(null, firstName, null, lastName, null);
    }

    public Person(String title, String firstName, String middleName, String lastName, String suffix) {
        this.title = blankIfNull(title);
        this.firstName = blankIfNull(firstName);
        this.middleName = blankIfNull(middleName);
        this.lastName = blankIfNull(lastName);
        this.suffix = blankIfNull(suffix);
    }

    public String getTitle() {
        return title;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSuffix() {
        return suffix;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + Objects.hashCode(this.title);
        hash = 61 * hash + Objects.hashCode(this.firstName);
        hash = 61 * hash + Objects.hashCode(this.middleName);
        hash = 61 * hash + Objects.hashCode(this.lastName);
        hash = 61 * hash + Objects.hashCode(this.suffix);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.middleName, other.middleName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        return Objects.equals(this.suffix, other.suffix);
    }

    @Override
    public String toString() {
        return getLastName() + "^" + getFirstName() + "^" + getMiddleName() + "^" + getTitle() + "^" + getSuffix();
    }

    public static Person parse(String personString) {
        String[] data = personString.split("\\^");
        String lastName = data.length > 0 ? data[0] : null;
        String firstName = data.length > 1 ? data[1] : null;
        String middleName = data.length > 2 ? data[2] : null;
        String title = data.length > 3 ? data[3] : null;
        String suffix = data.length > 4 ? data[4] : null;

        return new Person(title, firstName, middleName, lastName, suffix);
    }

    private static String blankIfNull(String input) {
        return input == null ? "" : input;
    }
}
