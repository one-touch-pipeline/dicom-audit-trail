/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.xml.layer;

import de.dkfz.odcf.audit.*;
import de.dkfz.odcf.audit.xml.*;
import java.util.*;

import static de.dkfz.odcf.audit.xml.layer.ParticipantObjectIdentification.ObjectTypeCodeRole.*;

/**
 * A layer class representing a DICOM ParticipantObjectIdentification.<br>
 * Required attributes:<br>
 * - ParticipantObjectID<br>
 * Required children:<br>
 * - ParticipantObjectIDTypeCode<br>
 * - ParticipantObjectName<br>
 * <br>
 * Possible ParticipantObjectTypeCode / TypeCodeRole combinations:<br>
 * TypeCode PERSON: PATIENT, RESOURCE, USER, DOCTOR, GUARANTOR,
 * SECURITY_USER<br>
 * TypeCode SYSTEM_OBJECT: REPORT, USER, LIST, SECURITY_USER,
 * SECURITY_USER_GROUP,<br>
 * SECURITY_RESOURCE, SECURITY_GRANULARITY_DEFINITION, DATA_DESTINATION,
 * DATA_REPOSITORY, SCHEDULE, JOB, JOB_STREAM<br>
 * TypeCode ORGANIZATION: LOCATION, RESOURCE, SUBSCRIBER, GUARANTOR, PROVIDER,
 * CUSTOMER<br>
 * TypeCode OTHER: Any<br>
 *
 * @author Florian Tichawa
 */
public class ParticipantObjectIdentification extends DynamicXmlObject<ParticipantObjectIdentification> {

    public enum ObjectTypeCode {
        PERSON(1, PATIENT, RESOURCE, USER, DOCTOR, GUARANTOR, SECURITY_USER),
        SYSTEM_OBJECT(2, REPORT, USER, LIST, SECURITY_USER, SECURITY_USER_GROUP, SECURITY_RESOURCE, SECURITY_GRANULARITY_DEFINITION, DATA_DESTINATION, DATA_REPOSITORY, SCHEDULE, JOB, JOB_STREAM),
        ORGANIZATION(3, LOCATION, RESOURCE, SUBSCRIBER, GUARANTOR, PROVIDER, CUSTOMER),
        OTHER(4);

        private final int numVal;
        private final ObjectTypeCodeRole[] validRoles;

        ObjectTypeCode(int numVal, ObjectTypeCodeRole... validRoles) {
            this.numVal = numVal;
            this.validRoles = validRoles;
        }

        public int intVal() {
            return numVal;
        }

        public boolean isValidFor(ObjectTypeCodeRole role) {
            return this == OTHER || Arrays.stream(validRoles)
                    .anyMatch(validRole -> validRole == role);
        }

        @Override
        public String toString() {
            return String.valueOf(intVal());
        }
    }

    public enum ObjectTypeCodeRole {
        PATIENT(1), RESOURCE(4), USER(6), DOCTOR(8), GUARANTOR(10), SECURITY_USER(11), PROVIDER(15),
        REPORT(3), LIST(7), SECURITY_USER_GROUP(12), SECURITY_RESOURCE(13), SECURITY_GRANULARITY_DEFINITION(14),
        DATA_DESTINATION(16), DATA_REPOSITORY(17), SCHEDULE(18), JOB(20), JOB_STREAM(21),
        LOCATION(2), SUBSCRIBER(9), CUSTOMER(19);

        private final int numVal;

        ObjectTypeCodeRole(int numVal) {
            this.numVal = numVal;
        }

        public int intVal() {
            return numVal;
        }

        @Override
        public String toString() {
            return String.valueOf(intVal());
        }
    }

    public enum ObjectIdTypeCode {
        MEDICAL_RECORD_NUMBER(1), PATIENT_NUMBER(2), ENCOUNTER_NUMBER(3),
        ENROLLEE_NUMBER(4), SOCIAL_SECURITY_NUMBER(5), ACCOUNT_NUMBER(6),
        GUARANTOR_NUMBER(7), REPORT_NAME(8), REPORT_NUMBER(9),
        SEARCH_CRITERIA(10), USER_IDENTIFIER(11), URI(12),
        STUDY(110180), SOP(110181), NODE(110182);

        private final int numVal;

        ObjectIdTypeCode(int numVal) {
            this.numVal = numVal;
        }

        public int intVal() {
            return numVal;
        }

        @Override
        public String toString() {
            return String.valueOf(intVal());
        }
    }

    public enum ObjectLifeCycle {
        ORIGINATION(1), IMPORT_COPY(2), AMENDMENT(3), VERIFICATION(4), TRANSLATION(5),
        ACCESS_USE(6), DEIDENTIFICATION(7), AGGREGATION(8), REPORT(9), EXPORT(10),
        DISCLOSURE(11), DISCLOSURE_RECEIPT(12), ARCHIVING(13), DELETION(14), ERASURE(15);

        private final int numVal;

        ObjectLifeCycle(int numVal) {
            this.numVal = numVal;
        }

        public int intVal() {
            return numVal;
        }

        @Override
        public String toString() {
            return String.valueOf(intVal());
        }
    }

    /**
     * Default constructor.
     */
    public ParticipantObjectIdentification() {
        super("ParticipantObjectIdentification");
        withReqAttr("ParticipantObjectID");
        withReqChild("ParticipantObjectIDTypeCode");
        withReqChild("ParticipantObjectName");
    }

    /**
     * Sets this instance's ParticipantObjectID.
     *
     * @param id ParticipantObjectID to set
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withId(String id) {
        return withAttr("ParticipantObjectID", id);
    }

    /**
     * Shorthand method for withIdTypeCode(csdCode, csdCode.name()).
     *
     * @param csdCode Code according to DCM
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withIdTypeCode(ObjectIdTypeCode csdCode) {
        return withIdTypeCode(csdCode, csdCode.name());
    }

    /**
     * Shorthand method for withIdTypeCode(csdCode, "DCM", originalText).
     *
     * @param csdCode TypeCode according to DCM
     * @param originalText TypeCode name
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withIdTypeCode(ObjectIdTypeCode csdCode, String originalText) {
        return withIdTypeCode(csdCode, "DCM", originalText, null);
    }

    /**
     * Shorthand method for withIdTypeCode(csdCode, csn, originalText, null).
     *
     * @param csdCode TypeCode according to given CSN
     * @param csn CodeSystemName
     * @param originalText TypeCode name
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withIdTypeCode(ObjectIdTypeCode csdCode, String csn, String originalText) {
        return withIdTypeCode(csdCode, csn, originalText, null);
    }

    /**
     * Adds a ParticipantObjectIDTypeCode to this instance.
     *
     * @param csdCode TypeCode according to given CSN
     * @param csn CodeSystemName
     * @param originalText TypeCode name
     * @param displayName Human readable TypeCode name
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withIdTypeCode(ObjectIdTypeCode csdCode, String csn, String originalText, String displayName) {
        return withChild(AuditFactory.createElementWithCodeSystemAttributes("ParticipantObjectIDTypeCode", csdCode.toString(), csn, originalText, displayName));
    }

    /**
     * Sets this instance's ParticipantObjectName child node.
     *
     * @param name Name to set
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withName(String name) {
        return withChild(AuditFactory.createElement("ParticipantObjectName").withContent(name));
    }

    /**
     * Sets this instance's ParticipantObjectQuery child node.
     *
     * @param query Query string to set
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withQueryString(String query) {
        withChild(AuditFactory.createElement("ParticipantObjectQuery").withContent(query));
        if (getChildren().stream().noneMatch(child -> child.getName().equals("ParticipantObjectName"))) {
            withChild(AuditFactory.createElement("ParticipantObjectName").withContent(query));
        }
        return this;
    }

    /**
     * Sets this instance's ParticipantTypeCode and ParticipantTypeCodeRole,
     * failing on illegal combinations.
     *
     * @param type ObjectTypeCode to set
     * @param role PbjectTypeCodeRole to set
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withTypeCodeRole(ObjectTypeCode type, ObjectTypeCodeRole role) {
        if (type.isValidFor(role)) {
            return withAttr("ParticipantObjectTypeCode", type)
                    .withAttr("ParticipantObjectTypeCodeRole", role);
        } else {
            throw new IllegalOperationException(this, role.name() + " is not a valid TypeCodeRole for TypeCode " + type.name());
        }
    }

    /**
     * Sets this instance's ParticipantObjectDataLifeCycle attribute.
     *
     * @param lifeCycle LifeCycle value to set
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withLifeCycle(ObjectLifeCycle lifeCycle) {
        return withAttr("ParticipantObjectDataLifeCycle", lifeCycle);
    }

    /**
     * Sets this instance's ParticipantObjectSensitivity attribute.
     *
     * @param sens Sensitivity value
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withSensitivity(String sens) {
        return withAttr("ParticipantObjectSensitivity", sens);
    }

    /**
     * Adds a ParticipantObjectDetail child to this instance.
     *
     * @param name Detail name
     * @param value Detail value
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withDetail(String name, String value) {
        return withChild(AuditFactory.createElement("ParticipantObjectDetail").withAttr("type", name).withAttr("value", value));
    }

    /**
     * Adds a ParticipantObjectDescription child to this instance.
     *
     * @param description Description text
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withDescription(String description) {
        return withChild(AuditFactory.createElement("ParticipantObjectDescription").withContent(description));
    }

    /**
     * Adds an MMPS child to this instance.
     *
     * @param uid MMPS UID
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withMPPS(String uid) {
        return withChild(AuditFactory.createElement("MPPS").withAttr("UID", uid));
    }

    /**
     * Adds an Accession child to this instance.
     *
     * @param number AC number
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withAccession(String number) {
        return withChild(AuditFactory.createElement("Accession").withAttr("Number", number));
    }

    /**
     * Adds an SOPClass child with one Instance object for every method
     * call.<br>
     * Please do not add an SOP Instance any other way that with this method,
     * otherwise the class won't be able to track the NumberOfInstances
     * attribute correctly.<br>
     * The SOPClass UID entry is empty by default, which will not throw any
     * error. The recommended way to modify this value is via Selectors or
     * Constraints.
     *
     * @param uid SOP Instance UID
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withSopInstance(String uid) {
        DynamicXmlObject sopClass = getChildren().stream()
                .filter(child -> child.getName().equals("SOPClass"))
                .map(child -> (DynamicXmlObject) child)
                .findAny().orElse(AuditFactory.createElement("SOPClass").withAttr("UID", ""));
        if (!getChildren().contains(sopClass)) {
            withChild(sopClass);
        }
        Attribute<Integer> numOfInst = sopClass.getAttr("NumberOfInstances")
                .map(attr -> (Attribute<Integer>) attr)
                .map(attr -> new Attribute<>(attr.getName(), attr.getValue() + 1))
                .orElse(new Attribute<>("NumberOfInstances", 1));
        sopClass.withChild(AuditFactory.createElement("Instance").withAttr("UID", uid)).withAttr(numOfInst);
        return this;
    }

    /**
     * Adds a Study (Identified by a UID) to this instance.
     *
     * @param uid Study UID
     * @return This for chain calls
     */
    public ParticipantObjectIdentification withStudy(String uid) {
        DynamicXmlObject studies = getChildren().stream()
                .filter(child -> child.getName().equals("ParticipantObjectContainsStudy"))
                .map(child -> (DynamicXmlObject) child)
                .findAny().orElse(AuditFactory.createElement("ParticipantObjectContainsStudy"));
        if (!getChildren().contains(studies)) {
            withChild(studies);
        }
        studies.withChild(AuditFactory.createElement("StudyIDs").withAttr("UID", uid));
        return this;
    }

    /**
     * Marks this instance as encrypted or non encrypted. Default value is
     * false.
     *
     * @param encrypted Encryption flag
     * @return This for chain calls
     */
    public ParticipantObjectIdentification asEncrypted(boolean encrypted) {
        getChildren().stream()
                .filter(child -> child.getName().equals("Encrypted"))
                .forEach(this::removeChild);
        if (encrypted) {
            withChild(AuditFactory.createElement("Encrypted").withContent("true"));
        }
        return this;
    }

    /**
     * Marks this instance as anonymized or non anonymized. Default value is
     * false.
     *
     * @param anonymized Anonymization flag
     * @return This for chain calls
     */
    public ParticipantObjectIdentification asAnonymized(boolean anonymized) {
        getChildren().stream()
                .filter(child -> child.getName().equals("Anonymized"))
                .forEach(this::removeChild);
        if (anonymized) {
            withChild(AuditFactory.createElement("Anonymized").withContent("true"));
        }
        return this;
    }
}
