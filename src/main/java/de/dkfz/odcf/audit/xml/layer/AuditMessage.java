/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.xml.layer;

import de.dkfz.odcf.audit.xml.*;

/**
 * A layer class representing a DICOM AuditMessage.<br>
 * Required children:<br>
 * - EventIdentification<br>
 * - ActiveParticipant (At least one)<br>
 * - AuditSourceIdentification<br>
 *
 * @author Florian Tichawa
 * @param <T> Self reference
 */
public class AuditMessage<T extends AuditMessage<T>> extends DynamicXmlObject<T> {

    /**
     * Copy constructor. Uses the given instance as a template to create an new
     * identical AuditMessage.
     *
     * @param template AuditMessage instance to use as template
     */
    public AuditMessage(AuditMessage<T> template) {
        super(template);
    }

    /**
     * Default constructor.
     */
    public AuditMessage() {
        super("AuditMessage");
        withReqChild("EventIdentification");
        withReqChild("ActiveParticipant");
        withReqChild("AuditSourceIdentification");
    }

    /**
     * Shorthand method for withEventId(eventId, true).
     *
     * @param eventId The EventIdentification to add
     * @return This for chain calls
     */
    public T withEventId(EventIdentification eventId) {
        return withEventId(eventId, true);
    }

    /**
     * Sets this instance's EventIdentification.<br>
     * This method overrides any existing EventIdentification instances in this
     * instance if and only if override == true.
     *
     * @param eventId The EventIdentification to add
     * @param override Whether this function should override any existing
     * EventIDs
     * @return This for chain calls
     */
    protected T withEventId(EventIdentification eventId, boolean override) {
        return withOnlyChild(eventId, override);
    }

    /**
     * Searches and returns the EventIdentification of this instance, or null if
     * none exists.
     *
     * @return This instance's EventIdentification
     */
    public EventIdentification getEventId() {
        return getChildren().stream()
                .filter(child -> child instanceof EventIdentification)
                .map(child -> (EventIdentification) child)
                .findAny().orElse(null);
    }

    /**
     * Adds a new ActiveParticipant to this instance.<br>
     * By default, the first ActiveParticipant added is designated as
     * Requestor.<br>
     * This behaviour can be overridden by adding a new ActiveParticipant with
     * the isRequestor attribute set to true, <br>
     * which will in return remove the isRequestor attribute from all other
     * ActiveParticipants that are registered in this instance.<br>
     * <br>
     * Alternatively, an ActiveParticipant that has already been added to this
     * instance can also be set as Requestor by calling
     * ActiveParticipant.asRequestor(), overriding any other isRequestor
     * attributes.
     *
     * @param participant The ActiveParticipant to add
     * @return This for chain calls
     */
    public T withParticipant(ActiveParticipant participant) {
        withChild(participant);
        if (participant.isRequestor() || getChildren().stream().filter(child -> child instanceof ActiveParticipant).count() == 1) {
            participant.asRequestor();
        }
        return getThis();
    }

    /**
     * Shorthand method for withAuditSource(auditSource, true).
     *
     * @param auditSource The AuditSource to add
     * @return This for chain calls
     */
    public T withAuditSource(AuditSourceIdentification auditSource) {
        return withAuditSource(auditSource, true);
    }

    /**
     * Sets this instance's AuditSourceIdentification.<br>
     * Overrides any existing AuditSourceIdentification children if override ==
     * true.
     *
     * @param auditSource The new AuditSourceIdentification
     * @param override Whether to override the existing
     * AuditSourceIdentification.
     * @return This for chain calls
     */
    protected T withAuditSource(AuditSourceIdentification auditSource, boolean override) {
        return withOnlyChild(auditSource, override);
    }

    /**
     * Adds a new ParticipantObjectIdentification to this instance.
     *
     * @param participantObject ParticipantObjectIdentification to add
     * @return This for chain calls
     */
    public T withObject(ParticipantObjectIdentification participantObject) {
        return withChild(participantObject);
    }

    @Override
    protected boolean isFormatAndContentValid() {
        return super.isFormatAndContentValid()
                && getChildren().stream().filter(child -> child instanceof EventIdentification).count() == 1
                && getChildren().stream()
                        .filter(child -> child instanceof ActiveParticipant)
                        .map(child -> (ActiveParticipant) child)
                        .filter(ActiveParticipant::isRequestor)
                        .count() == 1;
    }
}
