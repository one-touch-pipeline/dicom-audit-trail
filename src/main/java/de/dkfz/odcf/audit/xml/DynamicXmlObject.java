/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.xml;

/**
 * The core class of this library, representing a single XML tag. A
 * <code>DynmaicXmlObject</code> contains a list of it's required children and
 * attributes, as well as current child elements and attributes.
 *
 * It wraps <code>XmlObject</code> with chain-calling shorthand methods and
 * builds the base for all <code>de.dkfz.odcf.audit.xml.layer</code> classes.
 *
 * @author Florian Tichawa
 * @param <T> Self reference
 */
public class DynamicXmlObject<T extends DynamicXmlObject<T>> extends XmlObject {

    /**
     * A copy constructor, creating a deep copy of the given DynamicXmlObject.
     *
     * @param other DynamicXmlObject instance to copy
     */
    protected DynamicXmlObject(DynamicXmlObject<T> other) {
        super(other);
    }

    /**
     * Default constructor for XML tags.
     *
     * @param name Tag name
     */
    public DynamicXmlObject(String name) {
        super(name);
    }

    /**
     * Full option constructor for XML tags. The ordering priority overrides the
     * default ordering (By tag name), so that tags are sorted by their priority
     * first (Low to high), and by tag name afterwards (Alphabetic order). The
     * default priority is 0.
     *
     * @param name Tag name
     * @param priority Ordering priority override
     */
    public DynamicXmlObject(String name, int priority) {
        super(name, priority);
    }

    /**
     * Shorthand method for adding an empty required Attribute.
     *
     * @param attr Attribute name
     * @return This for chain calls
     */
    public final T withReqAttr(String attr) {
        return withAttr(new Attribute(attr, null, true));
    }

    /**
     * Shorthand method for
     * <code>withAttr(new Attribute(name, value, false))</code>.
     *
     * @param name Attribute name
     * @param value Attribute value
     * @return This for chain calls
     */
    public final T withAttr(String name, Object value) {
        return withAttr(new Attribute(name, value));
    }

    /**
     * Adds the given Attribute to this DynamicXMLObject.
     *
     * @param attr Attribute
     * @return This for chain calls
     */
    public final T withAttr(Attribute attr) {
        return withAttr(Selector.THIS, attr);
    }

    /**
     * Shorthand method for
     * <code>withAttr(selector, new Attribute(name, value, false))</code>.
     *
     * @param selector DOM Selector to define targets
     * @param name Attribute name
     * @param value Attribute value
     * @return This for chain calls
     */
    public final T withAttr(Selector selector, String name, Object value) {
        return withAttr(selector, new Attribute(name, value));
    }

    /**
     * Shorthand method for withAttr(selector, attr, true).
     *
     * @param selector DOM Selector to define targets
     * @param attr Attribute
     * @return This for chain calls
     */
    public final T withAttr(Selector selector, Attribute attr) {
        return withAttr(selector, attr, true);
    }

    /**
     * Adds a copy of the given Attribute to this and all children (Where
     * <code>selector</code> applies).
     *
     * @param selector DOM Selector to define targets
     * @param attr Attribute
     * @param override Whether the new Attribute should override any existing
     * Attribute
     * @return This for chain calls
     */
    public final T withAttr(Selector selector, Attribute attr, boolean override) {
        addAttribute(selector, attr, override);
        return getThis();
    }

    /**
     * Shorthand method for adding a required child.
     *
     * @param child Child name
     * @return This for chain calls
     */
    public final T withReqChild(String child) {
        getReqChildren().add(child);
        return getThis();
    }

    /**
     * Adds the given child to this element.
     *
     * @param child Child
     * @return This for chain calls
     */
    public final T withChild(XmlObject child) {
        addChild(child);
        return getThis();
    }

    /**
     * Adds the given child to this element after removing any other instances
     * of this class.
     *
     * @param child Child
     * @param override Whether the given child should override any existing
     * children or not
     * @return This for chain calls
     */
    public final T withOnlyChild(XmlObject child, boolean override) {
        for (int x = 0; x < getChildren().size(); x++) {
            XmlObject other = getChildren().get(x);
            if (other.getName().equals(child.getName())) {
                if (override) {
                    //Remove current element from the list
                    other.setParent(null);
                    //Reduce iteration counter because the current element was removed from the list
                    x--;
                } else {
                    return getThis();
                }
            }
        }
        addChild(child);
        return getThis();
    }

    /**
     * Sets this element's plain text content as required or not required.
     *
     * @param req ContentIsRequired flag
     * @return This for chain calls
     */
    public final T withReqContent(boolean req) {
        setReqContent(req);
        return getThis();
    }

    /**
     * Sets this element's plain text content.
     *
     * @param content New plain text content
     * @return This for chain calls
     */
    public final T withContent(String content) {
        setContent(content);
        return getThis();
    }

    /**
     * Shorthand method for <code>new DynamicXmlObject(this)</code>
     *
     * @return A deep copy of this DynamicXmlObject
     */
    @Override
    public T copy() {
        return (T) new DynamicXmlObject<>(this);
    }

    /**
     * Generic replacement for <code>this</code>.
     *
     * @return this, casted to it's actual type.
     */
    protected T getThis() {
        return (T) this;
    }
}
