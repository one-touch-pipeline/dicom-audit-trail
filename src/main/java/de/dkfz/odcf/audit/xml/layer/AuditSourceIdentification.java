/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.xml.layer;

import de.dkfz.odcf.audit.*;
import de.dkfz.odcf.audit.xml.*;

/**
 * A layer class representing a DICOM AuditSourceIdentification.<br>
 * Required attributes:<br>
 * - AuditSourceTypeCode<br>
 * - AuditSourceID<br>
 * <br>
 * The current implementation only allows one TypeCode at once and uses the
 * given value as code attribute AND as AuditSourceTypeCode child to ensure
 * compatibility with as many implementations as possible.<br>
 * -&gt; See Issue #1
 *
 * @author Florian Tichawa
 */
public class AuditSourceIdentification extends DynamicXmlObject<AuditSourceIdentification> {

    public enum AuditSourceTypeCode {
        ENDUSER_DISPLAY(1), DATA_ACQUISION_DEV(2), WEB_SERVER(3), APPL_SERVER(4), DATABASE(5),
        SECURITY_SERVER(6), OSI_1_3_DEVICE(7), OSI_4_6_DEVICE(8), OTHER(9);

        private final int numVal;

        AuditSourceTypeCode(int numVal) {
            this.numVal = numVal;
        }

        public int intVal() {
            return numVal;
        }

        @Override
        public String toString() {
            return "" + intVal();
        }
    }

    /**
     * Default constructor.
     */
    public AuditSourceIdentification() {
        super("AuditSourceIdentification");
        withReqAttr("code");
        withAttr("codeSystemName", "DCM");
        withReqAttr("AuditSourceID");
        withReqChild("AuditSourceTypeCode");
    }

    /**
     * Sets this instance's AuditSourceTypeCode. The current implementation only
     * allows one TypeCode at once and uses the given value as code attribute
     * AND as AuditSourceTypeCode child to ensure compatibility with as many
     * implementations as possible.<br>
     * -&gt; See Issue #1
     *
     * @param code AuditSourceTypeCode to set
     * @return This for chain calls
     */
    public AuditSourceIdentification withTypeCode(AuditSourceTypeCode code) {
        withAttr("code", code);
        getChildren().stream()
                .filter(obj -> obj.getName().equals("AuditSourceTypeCode"))
                .forEach(this::removeChild);
        return withChild(AuditFactory.createElement("AuditSourceTypeCode").withContent(code.toString()));
    }

    /**
     * Sets this instance's CodeSystemName.
     *
     * @param csn CodeSystemName to set
     * @return This for chain calls
     */
    public AuditSourceIdentification withCodeSystemName(String csn) {
        return withAttr("codeSystemName", csn);
    }

    /**
     * Sets this instance's DisplayName.
     *
     * @param displayName DisplayName to set
     * @return This for chain calls
     */
    public AuditSourceIdentification withDisplayName(String displayName) {
        return withAttr("displayName", displayName);
    }

    /**
     * Sets this instance's OriginalText.
     *
     * @param originalText OriginalText to set
     * @return This for chain calls
     */
    public AuditSourceIdentification withOriginalText(String originalText) {
        return withAttr("originalText", originalText);
    }

    /**
     * Sets this instance's AuditEnterpriseID.
     *
     * @param enterpriseId AuditEnterpriseID to set
     * @return This for chain calls
     */
    public AuditSourceIdentification withAuditEnterpriseId(String enterpriseId) {
        return withAttr("AuditEnterpriseID", enterpriseId);
    }

    /**
     * Sets this instance's AuditSourceID.
     *
     * @param sourceId AuditSourceID to set
     * @return This for chain calls
     */
    public AuditSourceIdentification withAuditSourceId(String sourceId) {
        return withAttr("AuditSourceID", sourceId);
    }
}
