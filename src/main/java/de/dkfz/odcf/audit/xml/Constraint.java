/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.xml;

import de.dkfz.odcf.audit.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

/**
 * An autofill rule that can be used to preset values of XmlObjects.<br>
 * This class takes a Selector, a type argument and one or two additional
 * arguments, depending on the type.<br>
 * Please refer to the different constructors for further usage information.
 *
 * @author Florian Tichawa
 * @param <T> Class to apply this Constraint to
 */
public class Constraint<T extends XmlObject> implements Function<T, T> {

    public static enum Type {
        ATTRIBUTE, CHILD, CONTENT
    };

    private final Selector selector;
    private final String value;
    private final String name;
    private final Type type;

    /**
     * General constructor.<br>
     * This constructor expects a Selector, a type and one or two values to
     * specify the element to add.<br>
     * The Selector.stream() method is used to apply this constraint to all
     * matches.<br>
     * The attribute value is always required and defines the attribute name
     * (For Type.ATTRIBUTE), the tag name (For Type.CHILD) or the text body (For
     * Type.CONTENT).<br>
     * Type.CHILD and Type.CONTENT ignore the value field, but it is preferred
     * to either use null as value or to use the shorthand constructor for these
     * cases.<br>
     * <br>
     * Constraint implements Function&lt;XmlObject, XmlObject&gt; and returns
     * the (manipulated) XmlObject, making it possible to use this class with
     * Stream.map().
     *
     * @param selector Selector to filter out elements during Constraint
     * application
     * @param attribute Attribute / tag name or text body (Depending on type)
     * @param value Attribute value, this value is ignored for Type.CHILD and
     * Type.CONTENT
     * @param type Constraint type
     */
    private Constraint(Selector selector, String attribute, String value, Type type) {
        this.selector = selector;
        this.name = attribute;
        this.value = value;
        this.type = type;
    }

    /**
     * Shorthand attribute constructor.<br>
     * This constructor is equal to Constraint(selector, name, value,
     * Type.ATTRIBUTE).
     *
     * @param selector Selector to filter out elements during Constraint
     * application
     * @param name Attribute name
     * @param value Attribute value
     */
    public Constraint(String selector, String name, String value) {
        this(Selector.parse(selector), name, value, Type.ATTRIBUTE);
    }

    /**
     * Shorthand child and content constructor.<br>
     * This constructor is equal to Constraint(selector, name, null, type).<br>
     *
     * @param selector Selector to filter out elements during Constraint
     * application
     * @param name Tag name or Text body, depending on type
     * @param type Constraint type, only Type.CHILD and Type.CONTENT are
     * meaningful for this constructor
     */
    public Constraint(String selector, String name, Type type) {
        this(Selector.parse(selector), name, null, type);
    }

    /**
     * Returns this instance's Selector.
     *
     * @return This instance's Selector
     */
    public Selector getSelector() {
        return selector;
    }

    /**
     * Creates a new Attribute based on the given attribute name and value.
     *
     * @return New Attribute instance
     */
    public Attribute<?> getAttr() {
        if (value.toLowerCase().equals("true")) {
            return new Attribute<>(name, true).asTemplated(true);
        } else if (value.toLowerCase().equals("false")) {
            return new Attribute<>(name, false);
        } else if (value.matches("^\\d+$")) {
            return new Attribute<>(name, Integer.parseInt(value)).asTemplated(true);
        } else {
            return new Attribute<>(name, value).asTemplated(true);
        }
    }

    /**
     * Creates a new Child based on the given tag name.<br>
     * This method utilizes AuditFactory.createElement to construct a new
     * instance, resulting in potentially prebuilt DynamicXmlObjects with their
     * own Constraints.
     *
     * @return New DynamicXmlObject instance
     */
    public DynamicXmlObject getChild() {
        return AuditFactory.createElement(name);
    }

    /**
     * Recursively applies this Constraint to any match within the given root's
     * XML hierarchy.<br>
     * For Type.CHILD this method filters out the Selector matches first and
     * applies this Constraint to these matches afterwards to prevent infinite
     * recursions.
     *
     * @param root Root element to apply this Constraint to
     * @return Modified root for chain calls and / or Stream mapping
     */
    @Override
    public T apply(T root) {
        switch (type) {
            case ATTRIBUTE:
                selector.stream(root)
                        .filter(child -> !child.hasAttr(name) || !child.getAttr(name).filter(attr -> !attr.isTemplated()).isPresent())
                        .forEach(child -> child.addAttribute(getAttr()));
                break;
            case CHILD:
                List<XmlObject> list = selector.stream(root)
                        .collect(Collectors.toList());
                list.forEach(child -> child.addChild(getChild()));
                break;
            case CONTENT:
                selector.stream(root)
                        .forEach(child -> child.setContent(name));
                break;
        }
        return root;
    }
}
