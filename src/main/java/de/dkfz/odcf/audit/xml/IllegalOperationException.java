/*
 * Copyright (c) 2018 Florian Tichawa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.odcf.audit.xml;

/**
 * Custom exception for XML validation checks. Overrides the default
 * implementation of getMessage() to provide in-depth error information.
 *
 * @author Florian Tichawa
 */
public class IllegalOperationException extends IllegalArgumentException {

    private final XmlEntity source;

    /**
     * Default constructor.
     * 
     * @param source XmlEntity causing this Exception
     * @param message Message describing this Exception
     */
    public IllegalOperationException(XmlEntity source, String message) {
        super(message);
        this.source = source;
    }

    @Override
    public String getMessage() {
        return super.getMessage()
                + " (Source object: " + (source == null ? "Null Object" : source.getClass().getSimpleName())
                + " \"" + (source == null ? "null" : source.getName()) + "\")";
    }

    /**
     * Returns the source object causing this exception.<br>
     * This method is used to reduce the complexity of finding the source object
     * in the stack trace.
     *
     * @return
     */
    public XmlEntity getSource() {
        return source;
    }
}
