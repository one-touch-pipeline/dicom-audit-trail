# Documentation for the OTP DICOM Audit Library

This documentation is aimed at developers implementing the OTP DICOM Audit Library into a program for audit logging according to the [DICOM specification](http://dicom.nema.org/Dicom/2013/output/chtml/part15/sect_A.5.html).  
The [DCM code system](http://dicom.nema.org/medical/dicom/current/output/chtml/part16/chapter_D.html) is built into the OTP DICOM Audit Library, but the library does not check the supplied codes for validity, which means that the library can also be used with custom code systems.

## 1. General layout

   This library offers a lightweight XML implementation and wrappers for these XML objects to assist at building `AuditMessage` objects.  
   These `AuditMessage`s can be parsed into an XML representation that meets all requirements for a valid audit message as defined in the DICOM specification.  
   All classes used in representation of audit messages can be found in the `de.dkfz.odcf.dicom.audit.xml.layer` package and are subclasses of the `XmlObject` class to ensure parsability into the XML format.
   The AuditFactory class offers the possibility to store and load templates (Partially prebuilt AuditMessage instances) from a template library, which makes it easier to submit predefined audit messages.  
   The default template library is loaded during the startup of this library, but custom templates can be injected by supplying one or more template libraries via `AuditFactory#loadTemplates` before doing any other calls related to this framework.  
   Overriding an existing template is forbidden due to security reasons, but existing templates can always be extended by custom templates.

## 2. UIDs and the OID

   Some fields in the DICOM standard require a UID (globally unique identifier), which should consist of a OID (Object Identifier, issued from an organization like DIMDI) prefix, followed by a unique (for the given OID namespace) dotted-integer identifier.  
   The default implementation of this library (designed for otp@dkfz-heidelberg.de) offers the `OtpDicomAuditFactory#generateUID` method that generates a UID from any (local) identifier according to this specification. It is highly recommended to use a similar helper method to ensure uniqueness of the generated UIDs.  
   Every field requiring a UID in the default implementation is called `<fieldName>UID` in the method signature and contains the keyword "UID" in the JavaDoc parameter description.

## 3. AuditFactory

   The `AuditFactory` is the main entry point of this library. Custom template libraries can be injected into the factory by calling `AuditFactory#loadTemplates` before creating the first AuditMessages.  
   If no library was supplied, then the Factory will default to the internal library.  
   `AuditFactory#ofType` attempts to generate an `AuditMessage` from the template library, using the supplied name as identifier and throws an `IllegalArgumentException` on failure.  
   `AuditFactory#createElement` is used to load elements from the internal element library and is the recommended low level way of adding new children to existing `XmlObject`s.

## 4. Selector

   The `Selector` class provides XPath-like navigation in an `XmlObject` tree and is used internally for the template library. `Selector`s can be used programmatically as well, but this is not recommended due to the missing type safety.

## 5. AuditMessage

   The `AuditMessage` class represents DICOM audit messages and can be acquired by manual (via the constructor, followed by manual tree construction, see `XmlObject#addChild` in the JavaDoc) or automated (via the template library and `AuditFactory#ofType` calls) construction.  
   An `AuditMessage` instance needs one child of each the `EventIdentification`, `ActiveParticipant` and `AuditSourceIdentification` classes, but it can contain additional `ActiveParticipant`s plus any number of `ParticipantObject`s.  
   `AuditMessage`s do not have any attributes.  
   An `AuditMessage` instance can be converted into its string representation by calling `XmlObject#toString` or `XmlObject#toXmlString` (for pretty print), and it will automatically check itself for conformity with the XSD specification of the DICOM audit standard during this process, throwing a detailed `IllegalOperationException` on any validation errors.

## 6. EventIdentification

   The `EventIdentification` class describes the event type and outcome of its parent `AuditMessage` ("WHAT happened?").  
   This information consists of "action type", which is defined by the `EventActionCode` attribute (**C**reate, **R**ead, **U**pdate, **D**elete, **E**xecute), and the `EventID` / `EventTypeCode` child pair.  
   The `EventID` describes the exact event (e.g. "User login") according to any supplied code system, while the `EventTypeCode` describes the "event group" (e.g. "Authentification", which contains both "User Login" and "User Logout").  
   Every `EventIdentification` furthermore needs an `EventDateTime` timestamp and an `EventOutcomeIndicator` (0 = Success, 4 = Minor Failure, 8 = Major Failure, 12 = Critical Failure).

## 7. ActiveParticipant

   The `ActiveParticipant` class represents an active contributor for the logged action (e.g. "WHO did it?").  
   A participant is identified by the UserID attribute, which is preferred (but not required) to be a domain name like `<user>@<domain>` or `<user>.<domain>` if applicable.  
   Every `ActiveParticipant` should have a `RoleIDCode` child to describe the participant's role in the current event.  
   An AuditMessage always has exactly one requestor of the logged event, and by default the first ActiveParticipant added to an AuditMessage is assumed to be the requestor.  
   This behaviour can be overridden by using `ActiveParticipant#asRequestor` on any participant (before or after adding it to the AuditMessage).  
   Adding or setting a new requestor will demote any existing requestors to non-requestor participants to ensure that every AuditMessage has exactly one requestor.

## 8. AuditSourceIdentification

  The `AuditSourceIdentification` describes the source of an `AuditMessage` ("WHERE did it happen?").  
  It is identified by its `AuditSourceID` attribute, which has no specifications regarding its pattern, but it should conform to the `AuditSourceTypeCode` value, e.g. a URL for any server side applications.  
  The `AuditSourceTypeCode` is contained twice, once as the value of the `code` attribute, and once as `AuditSourceTypeCode` child due to a missing clarification about which field to use in the DICOM specification.  
  Both values share the same purpose, but it was decided to implement the TypeCode storage twice to ensure compatibility with as many other applications as possible. The recommended top-level method for setting the TypeCode (`AuditSourceIdentification#withTypeCode`) always sets both values, so there is no need to deal with this issue unless using any low-level API methods to set this value.  
  The `AuditSourceTypeCode` has to be picked from the DCM code system (or any other supplied code system).

## 9. ParticipantObjectIdentification

   A `ParticipantObjectIdentification` represents a resource or any comparable "inactive" component in the current event, like a document that has been edited or a user role that had been assigned and the user that this role had been assigned to ("With WHICH ITEM did it happen?").  
   The `ParticipantObjectIdentification` is identified by its `ParticipantObjectName`, `ParticipantObjectID` and `ParticipantObjectIDTypeCode`, with the last being picked from the DCM code system (or any other supplied code system).  
   The `ParticipantObjectTypeCode` and `ParticipantObjectTypeCodeRole` child pair describes the type of the object in question.  
   Just like the `EventTypeCode` and `EventTypeCodeRole`, these values have to be picked from a code system (e.g. DCM), and most code systems place tight restrictions on which TypeCode may be use with which TypeCodeRole, because in most cases the TypeCode describes the general object type, while the role assigns a certain function to this object (e.g. TypeCode "Person" with TypeCodeRole "Patient" from DCM).  
   The TypeCodeRole has to be picked from the list of valid TypeCodeRoles for each TypeCode, otherwise the operation will be rejected.
   Some `ParticipantObjectIdentification`s (e.g. documents) should have an `SOPClass` describing them.  
   The `SOPClass` describes the "type" of the document (e.g. "Which attributes does this object have? How can I access them? What can this object do?"), while the `InstanceID` represents one specific document.  
   The `SOPClass` child is a direct child of its `ParticipantObjectIdentification`, and the UID for the `SOPClass` has to be supplied as the `UID` attribute of the `SOPClass` element.
   `SOPClasses` have one or multiple `SOPInstance` children, defining the UID of the SOPClass instances (e.g. documents) being referenced.  
   Multiple ParticipantObjectIdentifications with the same SOPClass and one collective name / identifier may be grouped into one ParticipantObjectIdentification by adding all Instance UIDs to a single `ParticipantObjectIdentification`s SOPClass.
   SOPClass UIDs and SOPInstance UIDs must be globally unique, which can be ensured by using the already mentioned UID generator.

## 10. Template Library

   The template library is an XML file, its XSD scheme file is supplied in the config package.  
   It consists of multiple `<Template>` tags, containing any number of `<Attribute>` and either one `<Content>` or any number of `<Child>` tags.  
   Every `<Template>` represents one template that can be constructed by `AuditMessage#ofType`, the children are parsed to `Constraint`s, each defining a required (if no value was supplied in the template definition) or fixed (if a value was supplied in the template definition) `Attribute`, `Child` or plain text content for every element matching the `Selector`.  
   `Constraint`s are always resolved in order, starting at the first `Constraint` in the template definition and traversing down to the last `Constraint`.  
   Every `<Template>` can extend any other `<Template>`, which results in all `Constraint`s of the parent template being resolved before resolving the childs `Constraint`s. Template extension is recursive with no limitation.  
   **WARNING: Circular inheritance is not prevented by the parser and results in infinite loops!**
   `TemplatedAuditMessage` is an internal class extending `AuditMessage` and is used for templating and template inheritance only. Instances of this class should be modified via `Constraint`s **only**, as the combination of `Constraint` and `XmlObject` methods is not tested and may result in unexpected behaviour.  
   A `TemplatedAuditMessage` can be converted to a regular AuditMessage with `TemplatedAuditMessage#applyConstraints`, and it is recommended to never work with a `TemplatedAuditMessage` due to the already mentioned problems when combining `Constraint`s with `XmlObject` methods.

## License

This project is licensed under the [MIT license](LICENSE).

### ... of your contributions

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in dicom-audit-trail-impl by you, shall be licensed as MIT, without any additional
terms or conditions.
